package com.mfs.client.paypal.dto;

import java.util.Arrays;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthResponseDto {

	private String value;
	private Date expiration;
	private String tokenType;
	private String refreshToken;
	private String[] scope;
	private AdditionalInformation additionalInformation;

	private String detailMessage;
	private String[] stackTrace;
	private String[] suppressedExceptions;

	private String errorCode;
	private String errorMessage;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Date getExpiration() {
		return expiration;
	}

	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String[] getScope() {
		return scope;
	}

	public void setScope(String[] scope) {
		this.scope = scope;
	}

	public AdditionalInformation getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(AdditionalInformation additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public String getDetailMessage() {
		return detailMessage;
	}

	public void setDetailMessage(String detailMessage) {
		this.detailMessage = detailMessage;
	}

	public String[] getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(String[] stackTrace) {
		this.stackTrace = stackTrace;
	}

	public String[] getSuppressedExceptions() {
		return suppressedExceptions;
	}

	public void setSuppressedExceptions(String[] suppressedExceptions) {
		this.suppressedExceptions = suppressedExceptions;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return "AuthResponseDto [value=" + value + ", expiration=" + expiration + ", tokenType=" + tokenType
				+ ", refreshToken=" + refreshToken + ", scope=" + Arrays.toString(scope) + ", additionalInformation="
				+ additionalInformation + ", detailMessage=" + detailMessage + ", stackTrace="
				+ Arrays.toString(stackTrace) + ", suppressedExceptions=" + Arrays.toString(suppressedExceptions)
				+ ", errorCode=" + errorCode + ", errorMessage=" + errorMessage + "]";
	}

}
