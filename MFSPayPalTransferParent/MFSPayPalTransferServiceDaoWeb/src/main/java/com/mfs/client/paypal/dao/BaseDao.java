package com.mfs.client.paypal.dao;

import com.mfs.client.paypal.exception.CommonException;

public interface BaseDao {

	public boolean save(Object obj) throws CommonException;

	public boolean update(Object obj) throws CommonException;

	public boolean saveOrUpdate(Object obj) throws CommonException;

}
