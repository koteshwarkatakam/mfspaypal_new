package com.mfs.client.paypal.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.paypal.dao.SystemConfigDao;
import com.mfs.client.paypal.dao.TransactionDao;
import com.mfs.client.paypal.dto.AuthRequestDto;
import com.mfs.client.paypal.dto.AuthResponseDto;
import com.mfs.client.paypal.exception.CommonException;
import com.mfs.client.paypal.exception.DaoException;
import com.mfs.client.paypal.models.AccessTokenModel;
import com.mfs.client.paypal.service.AuthService;
import com.mfs.client.paypal.util.CallServices;
import com.mfs.client.paypal.util.CommonConstant;
import com.mfs.client.paypal.util.Encryption;
import com.mfs.client.paypal.util.HttpsConnectionRequest;
import com.mfs.client.paypal.util.HttpsConnectionResponse;
import com.mfs.client.paypal.util.JSONToObjectConversion;
import com.mfs.client.paypal.util.ResponseCodes;
import com.mfs.client.paypal.util.TokenExpirationCheck;

@Service("AuthService")
public class AuthServiceImpl implements AuthService {
	//private static final Logger LOGGER = Logger.getLogger(AuthServiceImpl.class);
	private static Logger LOGGER = LogManager.getLogger(AuthServiceImpl.class);
	@Autowired
	SystemConfigDao systemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	
	public AuthResponseDto getAccessToken() {
		AuthResponseDto response = null;
		AuthRequestDto request = new AuthRequestDto();
		AccessTokenModel authModel = null;
		Map<String, String> configMap = null;
		String serviceResponse = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectionResponse httpsConResult = null;
		Gson gson = new Gson();
		// getting data from config table
		try {
			configMap = systemConfigDao.getConfigDetailsMap();

			if (configMap == null) {

				response = new AuthResponseDto();
				response.setErrorCode(ResponseCodes.ER203.getCode());
				response.setErrorMessage(ResponseCodes.ER203.getMessage());

				return response;
			}
			authModel = transactionDao.getAuthTokenDetailsByLastRecord();
			// check null token
						if (authModel != null) {
						
							int tokenExpiredTime = CommonConstant.EXPIRATION_TIME;
							int tokenTime = TokenExpirationCheck.invalidSession(authModel);
							// check whether token expire or not 
							if (tokenTime <= tokenExpiredTime) {
								//token is not expire
								response = new AuthResponseDto();
								response.setValue(authModel.getTokenValue());
								return response;
							}
							
						}
						

			String xoomIssuedClientId = configMap.get(CommonConstant.CLIENT_ID);
			String xoomIssuedClientSecret = configMap.get(CommonConstant.CLIENT_SECRET);

			String token = Encryption.encryption(xoomIssuedClientId, xoomIssuedClientSecret);

			request.setUsername(configMap.get(CommonConstant.XOOM_ISSUED_USERNAME));
			request.setPassword(configMap.get(CommonConstant.XOOM_ISSUED_PASSWORD));
			request.setGrant_type(configMap.get(CommonConstant.XOOM_ISSUED_GRANT_TYPE));

			String partnerRequest = gson.toJson(request);

			String serviceSwitch = configMap.get(CommonConstant.SWITCH);
			if (serviceSwitch.equalsIgnoreCase("on")) {

				headerMap = new HashMap<String, String>();
				headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_X_WWW_FORM_URLENCODED);
				headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BASIC + token);
				connectionRequest.setHeaders(headerMap);

				// set service URL

				connectionRequest.setHttpmethodName("POST");
				if (configMap.get(CommonConstant.PORT) != null) {
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
				}
				connectionRequest.setServiceUrl(
						configMap.get(CommonConstant.BASE_URL) + configMap.get(CommonConstant.AUTH_ENDPOINT));

				// send request to partner
				httpsConResult = CallServices.getResponseFromService(connectionRequest, partnerRequest);

			} else {

				serviceResponse = "{\r\n"
						+ "  \"value\": \"H4sIAAAAAAAAAB2P3W6DIABG36hRG5d42Rp1sBV/ELTcNBU0IOic2lp9+tndf+ecfPUKZRVxFSsIyAZspMAE+szlPvgAeiipD71DvUJTf55U3AbOZQsc1JLt0vJ9SDdWwqbqwpnhHegzw49ZI0pkuPFyepRr7O/CLrRFJJ+8M9a9oBtohwoYmOQBbfLAS7BN09LOcErcPNMUl5Yb0lAkRM8NCab5WrymndPf5XkExtp9g3V/9zrzhY0+Es1iol0fRegs+tOCd3fWySHrz8nlv4/e/EOE3sDexzopeaTnq/MaWOFaQC2KO+jJI6JiMyneUcl8KJr0wJT34/9iOY4pq+fifruyZoK54aJZbvFY4PFRobXfLGf5A8vDagpLAQAA\",\r\n"
						+ "  \"expiration\": \"2021-09-15T00:22:07.876Z\",\r\n" + "  \"tokenType\": \"bearer\",\r\n"
						+ "  \"refreshToken\": null,\r\n" + "  \"scope\": [\r\n" + "    \"read\"\r\n" + "  ],\r\n"
						+ "  \"additionalInformation\": {\r\n" + "    \"jti\": \"iJJY7RFNRNB4cAvp0JROFhiFpO0\"\r\n"
						+ "  }\r\n" + "}";
				httpsConResult = new HttpsConnectionResponse();
				httpsConResult.setRespCode(ResponseCodes.S200.getCode() + "");
				httpsConResult.setResponseData(serviceResponse);

			}

			// check null response

			if (httpsConResult != null) {
				serviceResponse = httpsConResult.getResponseData();
			}
			LOGGER.info("AuthenticationServiceImpl in auth function response : " + serviceResponse);

			if (null == httpsConResult) {
				response = new AuthResponseDto();
				response.setErrorCode(ResponseCodes.NULL_RESPONSE.getCode());
				response.setErrorMessage(ResponseCodes.NULL_RESPONSE.getMessage());
			} else {

				if (httpsConResult.getRespCode().equals(ResponseCodes.S200.getCode() + "")) {
					response = (AuthResponseDto) JSONToObjectConversion.getObjectFromJson(serviceResponse,AuthResponseDto.class);
					logResponse(response);
				} else {
					response = new AuthResponseDto();
					response.setErrorCode(httpsConResult.getRespCode());
					response.setErrorMessage(httpsConResult.getTxMessage());

				}

			}
		} catch (CommonException ce) {
			ce.printStackTrace();
		}
		catch (DaoException de) {
			de.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	private void logResponse(AuthResponseDto response) throws CommonException, DaoException {
		AccessTokenModel authDetails = transactionDao.getAuthTokenDetailsByLastRecord();
		if (authDetails == null) {
			AccessTokenModel authModel = new AccessTokenModel();
			authModel.setTokenExpiration(response.getExpiration());
			authModel.setTokenType(response.getTokenType());
			authModel.setTokenValue(response.getValue());
			authModel.setDateLogged(new Date());
			transactionDao.save(authModel);

		} else {
			authDetails.setTokenExpiration(response.getExpiration());
			authDetails.setTokenType(response.getTokenType());
			authDetails.setTokenValue(response.getValue());
			authDetails.setDateLogged(new Date());
			transactionDao.update(authDetails);

		}

	}

}