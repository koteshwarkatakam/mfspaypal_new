package com.mfs.client.paypal.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HttpConnectorImpl {

	private static final Logger LOGGER = LogManager.getLogger(HttpConnectorImpl.class);

	public HttpsConnectionResponse httpUrlConnection(HttpsConnectionRequest httpRequest, String request)
			throws Exception {
		HttpURLConnection httpCon = null;
		URL url = null;

		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();
		try {
			url = new URL(httpRequest.getServiceUrl());

			httpCon = (HttpURLConnection) url.openConnection();

			httpCon.setRequestMethod(httpRequest.getHttpmethodName());

			httpCon.setReadTimeout(60000);

			httpCon.setDoOutput(true);
			httpCon.setDoInput(true);

			setHeaderPropertiesToHTTPSConnection(httpRequest, request, httpCon);

			if (request != null)

			{
				DataOutputStream wr = new DataOutputStream(httpCon.getOutputStream());

				wr.writeBytes(request);
				wr.flush();
				wr.close();
			}

			int responseCode = httpCon.getResponseCode();

			LOGGER.info("function response code" + responseCode);

			connectionResponse.setRespCode(responseCode + "");

			BufferedReader httpsResponse = null;

			if (responseCode == PayPalResponseCodes.S200.getCode() || responseCode == PayPalResponseCodes.S201.getCode()
					|| responseCode == PayPalResponseCodes.S202.getCode()) {

				httpsResponse = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));

			} else {

				String errMessage = getInputAsString(httpCon.getErrorStream());
				LOGGER.error("ER" + responseCode + " ErrorMessage " + errMessage);
				// update txMessage after getting actual error response
				connectionResponse.setTxMessage(errMessage);
				connectionResponse.setRespCode("ER" + responseCode);
				connectionResponse.setResponseData(errMessage);
			}

			String output = null;
			if (httpsResponse != null) {
				output = readConnectionDataWithBuffer(httpsResponse);
			}

			if (httpsResponse != null) {
				httpsResponse.close();
			}

			if (output != null) {

				connectionResponse.setResponseData(output);
			}

		} catch (Exception e) {
			throw new Exception(e);
		}

		return connectionResponse;
	}

	private void setHeaderPropertiesToHTTPSConnection(final HttpsConnectionRequest httpsConnectionRequest,
			String connectionData, HttpURLConnection httpsConnection) {

		setHeadersToHTTPSConnection(httpsConnectionRequest.getHeaders(), httpsConnection);

		if (connectionData != null && connectionData.length() > 0) {

			httpsConnection.setRequestProperty("Content-Length", String.valueOf(connectionData.length()));
		} else {
			httpsConnection.setRequestProperty("Content-Length", "0");
		}

	}

	private void setHeadersToHTTPSConnection(Map<String, String> headers, HttpURLConnection httpsConnection) {

		if (headers != null && !headers.isEmpty() && httpsConnection != null) {

			for (Entry<String, String> entry : headers.entrySet()) {

				httpsConnection.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}
	}

	private static String getInputAsString(InputStream paramInputStream) {
		Scanner localScanner = new Scanner(paramInputStream);
		return localScanner.useDelimiter("\\A").hasNext() ? localScanner.next() : "";
	}

	private String readConnectionDataWithBuffer(BufferedReader httpsResponse) throws IOException {
		String output;
		String responseLine;
		StringBuffer response = new StringBuffer();

		while ((responseLine = httpsResponse.readLine()) != null) {
			response.append(responseLine);
		}

		output = response.toString();
		return output;
	}
}