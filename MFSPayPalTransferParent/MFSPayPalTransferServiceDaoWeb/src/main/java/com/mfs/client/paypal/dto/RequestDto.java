package com.mfs.client.paypal.dto;

public class RequestDto {

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "RequestDto [id=" + id + "]";
	}
	
	
}
