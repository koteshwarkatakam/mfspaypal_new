package com.mfs.client.paypal.util;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mfs.client.paypal.dto.ResponseStatus;
import com.mfs.client.paypal.exception.CommonException;

public class CallServices {

	private static final Logger LOGGER = LogManager.getLogger(CallServices.class);

	public static HttpsConnectionResponse getResponseFromService(HttpsConnectionRequest connectionRequest,
			String request) throws CommonException {

		HttpsConnectionResponse httpsConResult = null;

		LOGGER.debug("url= " + connectionRequest.getServiceUrl() + " " + "request= " + request);

		try {

			// boolean isHttps = true;
			if (connectionRequest.getServiceUrl().contains("https")) {
				HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
				httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(request, connectionRequest);

			} else {
				HttpConnectorImpl httpConnectorImpl = new HttpConnectorImpl();
				httpsConResult = httpConnectorImpl.httpUrlConnection(connectionRequest, request);
			}
		} catch (Exception ce) {
			LOGGER.error("==>Exception in CallServices" + ce);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(ResponseCodes.ER220.getCode());
			status.setStatusMessage(ResponseCodes.ER220.getMessage());
			throw new CommonException(status);
		}
		return httpsConResult;
	}
}