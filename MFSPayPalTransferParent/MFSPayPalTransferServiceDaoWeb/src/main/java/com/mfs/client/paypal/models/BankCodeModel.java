package com.mfs.client.paypal.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="mfs_bank_code")
public class BankCodeModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "id")
	private int bankCodeId;
	
	@Column(name = "bank_name")
	private String bankName;
	@Column(name = "mfs_bank_code")
	private String bankCode;
	@Column(name = "country_code")
	private String countryCode;
	@Column(name = "currency_code")
	private String currencyCode;
	@Column(name = "iban")
	private String iban;
	@Column(name = "bic")
	private String bic;
	
	public int getBankCodeId() {
		return bankCodeId;
	}
	public void setBankCodeId(int bankCodeId) {
		this.bankCodeId = bankCodeId;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getCurrencyCode() {
		return currencyCode;
	}
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getBic() {
		return bic;
	}
	public void setBic(String bic) {
		this.bic = bic;
	}
	@Override
	public String toString() {
		return "BankCodeModel [bankCodeId=" + bankCodeId + ", bankName=" + bankName + ", bankCode=" + bankCode
				+ ", countryCode=" + countryCode + ", currencyCode=" + currencyCode + ", iban=" + iban + ", bic=" + bic
				+ "]";
	}
	
	
	
	
	
	

}
