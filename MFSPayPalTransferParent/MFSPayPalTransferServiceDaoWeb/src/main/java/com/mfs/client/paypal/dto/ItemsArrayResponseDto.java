package com.mfs.client.paypal.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ItemsArrayResponseDto {

	private TransferDto transfer;

	private RecipientDto recipient;

	private PaypalSenderDto sender;

	private List<AdditionalFieldsArray> additional_fields;

	public TransferDto getTransfer() {
		return transfer;
	}

	public void setTransfer(TransferDto transfer) {
		this.transfer = transfer;
	}

	public RecipientDto getRecipient() {
		return recipient;
	}

	public void setRecipient(RecipientDto recipient) {
		this.recipient = recipient;
	}

	public PaypalSenderDto getSender() {
		return sender;
	}

	public void setSender(PaypalSenderDto sender) {
		this.sender = sender;
	}

	public List<AdditionalFieldsArray> getAdditional_fields() {
		return additional_fields;
	}

	public void setAdditional_fields(List<AdditionalFieldsArray> additional_fields) {
		this.additional_fields = additional_fields;
	}

	@Override
	public String toString() {
		return "ItemsArrayResponseDto [transfer=" + transfer + ", recipient=" + recipient + ", sender=" + sender
				+ ", additional_fields=" + additional_fields + "]";
	}

}
