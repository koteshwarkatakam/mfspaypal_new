package com.mfs.client.paypal.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ProtocolException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;




public class HttpsConnectorImpl {

	private static final Logger LOGGER = LogManager.getLogger(HttpsConnectorImpl.class);

	private static final int CHECK_RESPONSE_CODE200 = 200;

	/**
	 * @param connectURL
	 * @param data
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws KeyManagementException
	 * @throws NoSuchAlgorithmException
	 * @throws ProtocolException
	 * @throws MTAdapterConEx
	 */

	public HttpsConnectionResponse connectionUsingHTTPS(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest)
			throws UnknownHostException, IOException, KeyManagementException, NoSuchAlgorithmException {
		SSLContext sslContext = null;

		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();
		sslContext = SSLContext.getInstance("TLSv1.2");
		sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
				new java.security.SecureRandom());

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());

		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		httpsConnection.setDoOutput(true);
		httpsConnection.setDoInput(true);
		httpsConnection.setReadTimeout(60000);

		if (connectionData != null) {
			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

			wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}
		int responseCode = httpsConnection.getResponseCode();

		LOGGER.info("Response Status from connecting partner -->" + responseCode);

		BufferedReader httpsResponse = null;

		if (responseCode == PayPalResponseCodes.S200.getCode()) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));
			connectionResponse.setRespCode(responseCode + "");

		} else if (responseCode == PayPalResponseCodes.ER403.getCode()) {
			connectionResponse.setRespCode("ER" + responseCode);
			connectionResponse.setTxMessage(ResponseCodes.FORBIDDEN.getMessage());
			connectionResponse.setResponseData(getInputAsString(httpsConnection.getErrorStream()));

		} else if (responseCode == PayPalResponseCodes.ER404.getCode()) {
			connectionResponse.setRespCode("ER" + responseCode);
			connectionResponse.setTxMessage(ResponseCodes.ENDPOINT_NOT_FOUND.getMessage());
			connectionResponse.setResponseData(getInputAsString(httpsConnection.getErrorStream()));

		} else if (responseCode == PayPalResponseCodes.ER500.getCode()) {
			connectionResponse.setRespCode("ER" + responseCode);
			connectionResponse.setTxMessage(PayPalResponseCodes.ER500.getMessage());
			connectionResponse.setResponseData(getInputAsString(httpsConnection.getErrorStream()));

		} else if (responseCode == PayPalResponseCodes.ER503.getCode()) {
			connectionResponse.setRespCode("ER" + responseCode);
			connectionResponse.setTxMessage(PayPalResponseCodes.ER503.getMessage());
			connectionResponse.setResponseData(getInputAsString(httpsConnection.getErrorStream()));

		} else {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.error("ErrorMessage " + errMessage);

			connectionResponse.setTxMessage(errMessage);
			connectionResponse.setRespCode("ER" + responseCode);
			connectionResponse.setResponseData(getInputAsString(httpsConnection.getErrorStream()));
		}

		String output = null;
		if (httpsResponse != null) {
			output = readConnectionDataWithBuffer(httpsResponse);

		}

		if (httpsResponse != null) {
			httpsResponse.close();
		}

		if (output != null) {
			connectionResponse.setResponseData(output);
		}
		LOGGER.debug("Response -->" + output);

		return connectionResponse;
	}

	private BufferedReader connectionUsingHTTPSRetry(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest)
			throws UnknownHostException, IOException, KeyManagementException, NoSuchAlgorithmException {
		SSLContext sslContext = null;

		sslContext = SSLContext.getInstance("SSL");
		sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
				new java.security.SecureRandom());

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());

		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

		httpsConnection.setReadTimeout(60000);

		httpsConnection.setDoOutput(true);
		httpsConnection.setDoInput(true);

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		if (connectionData != null) {

			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

			wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}

		int responseCode = httpsConnection.getResponseCode();

		LOGGER.info("Response Status from connecting partner -->" + responseCode);

		BufferedReader httpsResponse = null;

		if (responseCode == CHECK_RESPONSE_CODE200) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getErrorStream()));

		}

		String output = readConnectionDataWithBuffer(httpsResponse);

		if (output == null || output.isEmpty()) {
			if (responseCode == CHECK_RESPONSE_CODE200) {
				output = "OK";
			}
		}

		LOGGER.debug("End of connectionUsingHTTPSRetry() in HttpsConnectorImpl");
		return httpsResponse;
	}

	public static void wait(int seconds) throws InterruptedException {
		LOGGER.debug("Waiting " + seconds + " seconds ..");
		Thread.sleep(seconds * 1000);

	}

	private String readConnectionDataWithBuffer(BufferedReader httpsResponse) throws IOException {
		String output;
		String responseLine;
		StringBuffer response = new StringBuffer();

		while ((responseLine = httpsResponse.readLine()) != null) {
			response.append(responseLine);
		}

		output = response.toString();
		return output;
	}

	private void setHeaderPropertiesToHTTPSConnection(final HttpsConnectionRequest httpsConnectionRequest,
			String connectionData, HttpsURLConnection httpsConnection) {

		setHeadersToHTTPSConnection(httpsConnectionRequest.getHeaders(), httpsConnection);

		if (connectionData != null && connectionData.length() > 0) {

			httpsConnection.setRequestProperty("Content-Length", String.valueOf(connectionData.length()));
		} else {
			httpsConnection.setRequestProperty("Content-Length", "0");
		}

	}

	private void setHeadersToHTTPSConnection(Map<String, String> headers, HttpsURLConnection httpsConnection) {

		if (headers != null && !headers.isEmpty() && httpsConnection != null) {

			for (Entry<String, String> entry : headers.entrySet()) {

				httpsConnection.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}
	}

	private static String getInputAsString(InputStream paramInputStream) {
		Scanner localScanner = new Scanner(paramInputStream);
		return localScanner.useDelimiter("\\A").hasNext() ? localScanner.next() : "";
	}

}