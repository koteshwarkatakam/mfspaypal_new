package com.mfs.client.paypal.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.dto.Account;
import com.mfs.client.dto.BankRemitLogRequestDto;
import com.mfs.client.dto.Document;
import com.mfs.client.dto.Login;
import com.mfs.client.dto.Receive_amount;
import com.mfs.client.dto.Recipient;
import com.mfs.client.dto.Sender;
import com.mfs.client.dto.Status;
import com.mfs.client.paypal.dao.TransactionDao;
import com.mfs.client.paypal.dto.ItemsArrayResponseDto;
import com.mfs.client.paypal.exception.CommonException;
import com.mfs.client.paypal.service.MFSBankRemitLogService;
import com.mfs.client.paypal.util.CommonConstant;

@Service("MFSBankRemitLogService")
public class MFSBankRemitLogServiceImpl implements MFSBankRemitLogService {

	@Autowired
	TransactionDao transactionDao;

	public BankRemitLogRequestDto remmitRequest(ItemsArrayResponseDto request, Map<String, String> configValue)
			throws CommonException {

		BankRemitLogRequestDto mfsRequest = new BankRemitLogRequestDto();
		Document document = new Document();
		Document document1 = new Document();
		Recipient recipient = new Recipient();
		Receive_amount receive_amount = new Receive_amount();
		Login login = new Login();
		Sender sender = new Sender();
		String countryCode = null;

		login.setCorporate_code(configValue.get(CommonConstant.CORPORATE_CODE));
		login.setPassword(configValue.get(CommonConstant.PASSWORD));

		receive_amount.setAmount(request.getTransfer().getReceive_amount().getAmount());
		receive_amount.setCurrency_code(request.getTransfer().getReceive_amount().getCurrency_code());
		if (request.getSender().getAddress().getAddress1() != null
				&& request.getSender().getAddress().getAddress2() != null) {
			sender.setAddress(request.getSender().getAddress().getAddress1());
		} else {
			sender.setAddress("");
		}

		if (request.getSender().getAddress() != null && request.getSender().getAddress().getCity() != null) {
			sender.setCity(request.getSender().getAddress().getCity());
		} else {
			sender.setCity("");
		}

		// we dont get dob from paypal
		sender.setDate_of_birth("");

		if (request.getSender().getAddress().getCountry() != null) {
			document.setId_country(request.getSender().getAddress().getCountry());
		} else {
			document.setId_country("");
		}
		// we dont get dob from Id_expiry
		document.setId_expiry("");

		// we dont get dob from Id_number
		document.setId_number("");
		// we dont get dob from Id_type
		document.setId_type("");

		sender.setDocument(document);

		if (request.getSender().getEmail() != null) {
			sender.setEmail(request.getSender().getEmail());
		} else {
			sender.setEmail("");
		}

		// 2 digit country code
		// countryCode =
		// transactionDao.getTwoDigitCountryCodeByCountryCode(request.getTransaction().getSendCountryCode());
		if (request.getSender().getAddress().getCountry() != null) {
			sender.setFrom_country(request.getSender().getAddress().getCountry());
			sender.setMsisdn(request.getSender().getPhone());
		}
		sender.setName(request.getSender().getFirst_name());

		if (request.getSender().getAddress() != null && request.getSender().getAddress().getPostal_code() != null) {
			sender.setPostal_code(request.getSender().getAddress().getPostal_code());
		} else {
			sender.setPostal_code("");
		}

		if (request.getSender().getAddress() != null && request.getSender().getAddress().getState() != null) {
			sender.setState(request.getSender().getAddress().getState());
		} else {
			sender.setState("");
		}
		sender.setSurname(request.getSender().getLast_name());

		if (request.getRecipient().getAddress().getAddress1() != null
				&& request.getRecipient().getAddress().getAddress2() != null) {
			sender.setAddress(request.getRecipient().getAddress().getAddress1());
		} else {
			recipient.setAddress("");
		}
		if (request.getRecipient().getAddress() != null && request.getRecipient().getAddress().getCity() != null) {
			sender.setCity(request.getRecipient().getAddress().getCity());
		} else {
			recipient.setCity("");
		}

		recipient.setDate_of_birth("");

		if (request.getRecipient().getAddress().getCountry() != null) {
			document1.setId_country(request.getRecipient().getAddress().getCountry());

		} else {
			document1.setId_country("");
		}

		document1.setId_number("");

		document1.setId_type("");

		document1.setId_expiry("");
		recipient.setDocument(document1);

		if (request.getRecipient().getEmail() != null) {
			recipient.setEmail(request.getRecipient().getEmail());
		} else {
			recipient.setEmail("");
		}
		recipient.setMsisdn(request.getRecipient().getPhone());
		recipient.setName(request.getRecipient().getFirst_name());
		
		if (request.getRecipient().getAddress().getAddress1() != null) {
			recipient.setAddress(request.getRecipient().getAddress().getAddress1());
		}else{
			recipient.setAddress("");
		}
		
        if (request.getRecipient().getAddress().getCity() != null) {
        	recipient.setCity(request.getRecipient().getAddress().getCity());
		}else{
			recipient.setCity("");
		}
		
		if (request.getRecipient().getAddress().getPostal_code() != null) {
			recipient.setPostal_code(request.getRecipient().getAddress().getPostal_code());
		} else {
			recipient.setPostal_code("");
		}
		if (request.getRecipient().getAddress().getState() != null) {
			recipient.setState(request.getRecipient().getAddress().getState());
		} else {
			recipient.setState("");
		}
		Status status1 = new Status();
		status1.setCode("");
		recipient.setStatus(status1);
		recipient.setSurname(request.getRecipient().getLast_name());

		// 2 digit country code

		/*
		 * countryCode = transactionDao
		 * .getTwoDigitCountryCodeByCountryCode(request.getTransaction().
		 * getReceiveCountryCode()); if (countryCode != null) {
		 * recipient.setTo_country(countryCode); }
		 */
		recipient.setTo_country(request.getRecipient().getAddress().getCountry());
		Account account = new Account();
		account.setAccount_number(request.getTransfer().getAccount_number());

		// MFSBankDetail bankCode =
		// transactionDao.getMfsBankCodeByRouterCode(request.getRoutingCode());

		account.setMfs_bank_code(request.getTransfer().getBank_code());

		mfsRequest.setLogin(login);
		mfsRequest.setReceive_amount(receive_amount);
		mfsRequest.setSender(sender);
		mfsRequest.setRecipient(recipient);
		mfsRequest.setAccount(account);
		mfsRequest.setThird_party_trans_id(request.getTransfer().getTracking_number());

		/*
		 * if (request.getTransaction().getPurposeOfTransaction() != null &&
		 * !request.getTransaction().getPurposeOfTransaction().isEmpty()) {
		 * 
		 * // Get the MFS Transaction reason code from enum by MG code String
		 * ref = PurposeOfTrxEnum.getMFSCodeByMGCode(request.getTransaction().
		 * getPurposeOfTransaction()); mfsRequest.setReference(ref != null ? ref
		 * : "");
		 * 
		 * //mfsRequest.setReference(request.getTransaction().
		 * getPurposeOfTransaction());
		 * 
		 * } else { mfsRequest.setReference(""); }
		 */

		mfsRequest.setReference("");

		mfsRequest.setWsdl(configValue.get(CommonConstant.WSDL));
		if (configValue.get(CommonConstant.X_API_KEY) != null) {

			mfsRequest.setxApiKey(configValue.get(CommonConstant.X_API_KEY));

		}

		return mfsRequest;

	}

}
