package com.mfs.client.paypal.service;

import java.util.Map;

import com.mfs.client.dto.BankRemitLogRequestDto;
import com.mfs.client.paypal.dto.ItemsArrayResponseDto;
import com.mfs.client.paypal.exception.CommonException;

public interface MFSBankRemitLogService {
	

	public BankRemitLogRequestDto remmitRequest(ItemsArrayResponseDto request, Map<String, String> configValue)
			throws CommonException;

	
}
