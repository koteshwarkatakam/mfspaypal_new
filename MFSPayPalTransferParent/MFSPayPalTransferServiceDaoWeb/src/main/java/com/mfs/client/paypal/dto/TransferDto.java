package com.mfs.client.paypal.dto;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class TransferDto {

	private String account_number;

	private String account_name;

	private String account_type;

	private String routing_number;

	private String bank_code;

	private String bank_name;

	private String branch_code;

	private String branch_name;

	private String branch_district;

	private String sub_bank_code;

	private String sub_bank_name;

	private String tracking_number;

	private Date created;

	private SendAmountResponseDto send_amount;

	private ReceiveAmountResponseDto receive_amount;

	private FeeAmountResponseDto fee_amount;

	public String getAccount_number() {
		return account_number;
	}

	public void setAccount_number(String account_number) {
		this.account_number = account_number;
	}

	public String getAccount_name() {
		return account_name;
	}

	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}

	public String getAccount_type() {
		return account_type;
	}

	public void setAccount_type(String account_type) {
		this.account_type = account_type;
	}

	public String getRouting_number() {
		return routing_number;
	}

	public void setRouting_number(String routing_number) {
		this.routing_number = routing_number;
	}

	public String getBank_code() {
		return bank_code;
	}

	public void setBank_code(String bank_code) {
		this.bank_code = bank_code;
	}

	public String getBank_name() {
		return bank_name;
	}

	public void setBank_name(String bank_name) {
		this.bank_name = bank_name;
	}

	public String getBranch_code() {
		return branch_code;
	}

	public void setBranch_code(String branch_code) {
		this.branch_code = branch_code;
	}

	public String getBranch_name() {
		return branch_name;
	}

	public void setBranch_name(String branch_name) {
		this.branch_name = branch_name;
	}

	public String getBranch_district() {
		return branch_district;
	}

	public void setBranch_district(String branch_district) {
		this.branch_district = branch_district;
	}

	public String getSub_bank_code() {
		return sub_bank_code;
	}

	public void setSub_bank_code(String sub_bank_code) {
		this.sub_bank_code = sub_bank_code;
	}

	public String getSub_bank_name() {
		return sub_bank_name;
	}

	public void setSub_bank_name(String sub_bank_name) {
		this.sub_bank_name = sub_bank_name;
	}

	public String getTracking_number() {
		return tracking_number;
	}

	public void setTracking_number(String tracking_number) {
		this.tracking_number = tracking_number;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public SendAmountResponseDto getSend_amount() {
		return send_amount;
	}

	public void setSend_amount(SendAmountResponseDto send_amount) {
		this.send_amount = send_amount;
	}

	public ReceiveAmountResponseDto getReceive_amount() {
		return receive_amount;
	}

	public void setReceive_amount(ReceiveAmountResponseDto receive_amount) {
		this.receive_amount = receive_amount;
	}

	public FeeAmountResponseDto getFee_amount() {
		return fee_amount;
	}

	public void setFee_amount(FeeAmountResponseDto fee_amount) {
		this.fee_amount = fee_amount;
	}

	@Override
	public String toString() {
		return "TransferDto [account_number=" + account_number + ", account_name=" + account_name + ", account_type="
				+ account_type + ", routing_number=" + routing_number + ", bank_code=" + bank_code + ", bank_name="
				+ bank_name + ", branch_code=" + branch_code + ", branch_name=" + branch_name + ", branch_district="
				+ branch_district + ", sub_bank_code=" + sub_bank_code + ", sub_bank_name=" + sub_bank_name
				+ ", tracking_number=" + tracking_number + ", created=" + created + ", send_amount=" + send_amount
				+ ", receive_amount=" + receive_amount + ", fee_amount=" + fee_amount + "]";
	}

}
