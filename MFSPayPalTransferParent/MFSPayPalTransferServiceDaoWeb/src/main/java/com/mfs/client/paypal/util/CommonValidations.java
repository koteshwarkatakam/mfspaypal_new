package com.mfs.client.paypal.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonValidations {

	public boolean validateStringValues(String reqParamValue) {
		boolean validateResult = false;

		if (reqParamValue == null || reqParamValue.equals(" ")||reqParamValue.equals(""))
			validateResult = true;

		return validateResult;
	}

	public boolean validateAmountValues(Double amount) {
		boolean validateResult = false;
		if (amount == null)
			validateResult = true;

		return validateResult;
	}

	public boolean validateIntValues(int amount) {
		boolean validateResult = false;
		if (amount == 0)
			validateResult = true;

		return validateResult;
	}

	public boolean validateFloatValues(float amount) {
		boolean validateResult = false;
		if (amount == 0)
			validateResult = true;

		return validateResult;
	}

	public boolean validateNumber(String number) {
		boolean validateResult = false;
		if (number == null || !number.matches("[0-9]+"))
			validateResult = true;
		return validateResult;

	}

	public boolean isStringOnlyAlphabet(String str) {
		boolean validateResult = false;
		if ((str.equals("")) || (str == null) || (!str.matches("^[ A-Za-z0-9_@./#&+-]*$")))

			validateResult = true;

		return validateResult;
	}

	public boolean validateString(String reqParamValue) {
		boolean validateResult = false;
		Pattern pattern = Pattern.compile("^[ A-Za-z_@./#&+-]*$");
		if (pattern.matcher(reqParamValue).matches())
			validateResult = true;

		return validateResult;
	}

	public boolean validatePhoneNumber(String str) {
		boolean validateResult = false;
		String s = str.trim();
		if (s == null || s.isEmpty() || s.length() < 10 || s.length() > 12) {
			validateResult = true;
		} else if (s.length() == 11) {
			char c = s.charAt(0);

			if (c != '0') {
				validateResult = true;
			}
		} else {

			Pattern p = Pattern.compile("[^0-9]");
			Matcher m = p.matcher(s);
			boolean b = m.find();
			if (b == true) {
				validateResult = true;
			}
		}
		return validateResult;
	}

	public boolean validateAmount(String amount) {
		boolean validateResult = false;
		if (null == amount || amount.equals(" ") || amount.equals("") || Double.parseDouble(amount) <= 0)
			validateResult = true;
		return validateResult;
	}

	/*
	 * public boolean validateAmountValue(String amount) { Double amount1 =
	 * Double.parseDouble(amount); boolean validateResult = false; if (amount1 ==
	 * null || amount1 <= 0.0) validateResult = true; return validateResult; }
	 */
}
