package com.mfs.client.paypal.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "transfer_detail_log")
public class TransferDetailsLogModel {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int transferLogId;

	@Column(name = "mfs_trans_id")
	private String mfsTransId;

	@Column(name = "status")
	private String status;

	@Column(name = "resp_code")
	private String respCode;

	@Column(name = "resp_message")
	private String respMessage;

	@Column(name = "date_logged")
	private Date dateLogged;

	@Column(name = "date_updated")
	private Date dateUpdated;

	@Column(name = "account_number")
	private String accountNumber;

	@Column(name = "account_name")
	private String accountName;

	@Column(name = "account_type")
	private String accountType;

	@Column(name = "routing_number")
	private String routingNummber;

	@Column(name = "bank_code")
	private String bankCode;

	@Column(name = "bank_name")
	private String bankName;

	@Column(name = "tracking_number")
	private String trackingNumber;

	@Column(name = "send_amount")
	private String sendAmount;

	@Column(name = "send_cur_code")
	private String sendCurCode;

	@Column(name = "receive_amount")
	private String receiveAmount;

	@Column(name = "rec_cur_code")
	private String recCurCode;

	@Column(name = "fee_amount")
	private String feeAmount;

	@Column(name = "fee_cur_code")
	private String feeCurCode;

	@Column(name = "receiver_first_name")
	private String receiverFirstName;

	@Column(name = "receiver_last_name")
	private String receiverLastName;

	@Column(name = "receiver_middle_name")
	private String receiverMiddleName;

	@Column(name = "receiver_msisdn")
	private String receiverMsisdn;

	@Column(name = "receiver_country")
	private String receiverCountry;

	@Column(name = "sender_first_name")
	private String senderFirstName;

	@Column(name = "sender_last_name")
	private String senderLastName;

	@Column(name = "sender_country")
	private String senderCountry;

	@Column(name = "retry_count")
	private int retryCount;

	@Column(name = "paypal_status")
	private String paypalStatus;
	
	@Column(name = "paypal_status_code")
	private String paypalStatusCode;

	@Column(name = "mfs_status")
	private String mfsStatus;

	@Column(name = "sender_address")
	private String senderAddress;

	@Column(name = "sender_city")
	private String senderCity;

	@Column(name = "sender_email")
	private String senderEmail;

	@Column(name = "sender_msisdn")
	private String senderMsisdn;

	@Column(name = "sender_postal_code")
	private String senderPostalCode;

	@Column(name = "sender_state")
	private String senderState;
	
	@Column(name = "receiver_address")
	private String receiverAddress;
	
	@Column(name = "receiver_city")
	private String receiverCity;
	
	@Column(name = "receiver_email")
	private String receiverEmail;
	
	@Column(name = "receiver_postal_code")
	private String receiverPostalCode;
	
	@Column(name = "receiver_state")
	private String receiverState;

	public int getTransferLogId() {
		return transferLogId;
	}

	public void setTransferLogId(int transferLogId) {
		this.transferLogId = transferLogId;
	}

	public String getMfsTransId() {
		return mfsTransId;
	}

	public void setMfsTransId(String mfsTransId) {
		this.mfsTransId = mfsTransId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getRespMessage() {
		return respMessage;
	}

	public void setRespMessage(String respMessage) {
		this.respMessage = respMessage;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getRoutingNummber() {
		return routingNummber;
	}

	public void setRoutingNummber(String routingNummber) {
		this.routingNummber = routingNummber;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getTrackingNumber() {
		return trackingNumber;
	}

	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}

	public String getSendAmount() {
		return sendAmount;
	}

	public void setSendAmount(String sendAmount) {
		this.sendAmount = sendAmount;
	}

	public String getSendCurCode() {
		return sendCurCode;
	}

	public void setSendCurCode(String sendCurCode) {
		this.sendCurCode = sendCurCode;
	}

	public String getReceiveAmount() {
		return receiveAmount;
	}

	public void setReceiveAmount(String receiveAmount) {
		this.receiveAmount = receiveAmount;
	}

	public String getRecCurCode() {
		return recCurCode;
	}

	public void setRecCurCode(String recCurCode) {
		this.recCurCode = recCurCode;
	}

	public String getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(String feeAmount) {
		this.feeAmount = feeAmount;
	}

	public String getFeeCurCode() {
		return feeCurCode;
	}

	public void setFeeCurCode(String feeCurCode) {
		this.feeCurCode = feeCurCode;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastName() {
		return receiverLastName;
	}

	public void setReceiverLastName(String receiverLastName) {
		this.receiverLastName = receiverLastName;
	}

	public String getReceiverMiddleName() {
		return receiverMiddleName;
	}

	public void setReceiverMiddleName(String receiverMiddleName) {
		this.receiverMiddleName = receiverMiddleName;
	}

	public String getReceiverMsisdn() {
		return receiverMsisdn;
	}

	public void setReceiverMsisdn(String receiverMsisdn) {
		this.receiverMsisdn = receiverMsisdn;
	}

	public String getReceiverCountry() {
		return receiverCountry;
	}

	public void setReceiverCountry(String receiverCountry) {
		this.receiverCountry = receiverCountry;
	}

	public String getSenderFirstName() {
		return senderFirstName;
	}

	public void setSenderFirstName(String senderFirstName) {
		this.senderFirstName = senderFirstName;
	}

	public String getSenderLastName() {
		return senderLastName;
	}

	public void setSenderLastName(String senderLastName) {
		this.senderLastName = senderLastName;
	}

	public String getSenderCountry() {
		return senderCountry;
	}

	public void setSenderCountry(String senderCountry) {
		this.senderCountry = senderCountry;
	}

	public int getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	public String getPaypalStatus() {
		return paypalStatus;
	}

	public void setPaypalStatus(String paypalStatus) {
		this.paypalStatus = paypalStatus;
	}

	public String getPaypalStatusCode() {
		return paypalStatusCode;
	}

	public void setPaypalStatusCode(String paypalStatusCode) {
		this.paypalStatusCode = paypalStatusCode;
	}

	public String getMfsStatus() {
		return mfsStatus;
	}

	public void setMfsStatus(String mfsStatus) {
		this.mfsStatus = mfsStatus;
	}

	public String getSenderAddress() {
		return senderAddress;
	}

	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}

	public String getSenderCity() {
		return senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}

	public String getSenderEmail() {
		return senderEmail;
	}

	public void setSenderEmail(String senderEmail) {
		this.senderEmail = senderEmail;
	}

	public String getSenderMsisdn() {
		return senderMsisdn;
	}

	public void setSenderMsisdn(String senderMsisdn) {
		this.senderMsisdn = senderMsisdn;
	}

	public String getSenderPostalCode() {
		return senderPostalCode;
	}

	public void setSenderPostalCode(String senderPostalCode) {
		this.senderPostalCode = senderPostalCode;
	}

	public String getSenderState() {
		return senderState;
	}

	public void setSenderState(String senderState) {
		this.senderState = senderState;
	}

	public String getReceiverAddress() {
		return receiverAddress;
	}

	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}

	public String getReceiverCity() {
		return receiverCity;
	}

	public void setReceiverCity(String receiverCity) {
		this.receiverCity = receiverCity;
	}

	public String getReceiverEmail() {
		return receiverEmail;
	}

	public void setReceiverEmail(String receiverEmail) {
		this.receiverEmail = receiverEmail;
	}

	public String getReceiverPostalCode() {
		return receiverPostalCode;
	}

	public void setReceiverPostalCode(String receiverPostalCode) {
		this.receiverPostalCode = receiverPostalCode;
	}

	public String getReceiverState() {
		return receiverState;
	}

	public void setReceiverState(String receiverState) {
		this.receiverState = receiverState;
	}

	@Override
	public String toString() {
		return "TransferDetailsLogModel [transferLogId=" + transferLogId + ", mfsTransId=" + mfsTransId + ", status="
				+ status + ", respCode=" + respCode + ", respMessage=" + respMessage + ", dateLogged=" + dateLogged
				+ ", dateUpdated=" + dateUpdated + ", accountNumber=" + accountNumber + ", accountName=" + accountName
				+ ", accountType=" + accountType + ", routingNummber=" + routingNummber + ", bankCode=" + bankCode
				+ ", bankName=" + bankName + ", trackingNumber=" + trackingNumber + ", sendAmount=" + sendAmount
				+ ", sendCurCode=" + sendCurCode + ", receiveAmount=" + receiveAmount + ", recCurCode=" + recCurCode
				+ ", feeAmount=" + feeAmount + ", feeCurCode=" + feeCurCode + ", receiverFirstName=" + receiverFirstName
				+ ", receiverLastName=" + receiverLastName + ", receiverMiddleName=" + receiverMiddleName
				+ ", receiverMsisdn=" + receiverMsisdn + ", receiverCountry=" + receiverCountry + ", senderFirstName="
				+ senderFirstName + ", senderLastName=" + senderLastName + ", senderCountry=" + senderCountry
				+ ", retryCount=" + retryCount + ", paypalStatus=" + paypalStatus + ", paypalStatusCode="
				+ paypalStatusCode + ", mfsStatus=" + mfsStatus + ", senderAddress=" + senderAddress + ", senderCity="
				+ senderCity + ", senderEmail=" + senderEmail + ", senderMsisdn=" + senderMsisdn + ", senderPostalCode="
				+ senderPostalCode + ", senderState=" + senderState + ", receiverAddress=" + receiverAddress
				+ ", receiverCity=" + receiverCity + ", receiverEmail=" + receiverEmail + ", receiverPostalCode="
				+ receiverPostalCode + ", receiverState=" + receiverState + "]";
	}

	
}
