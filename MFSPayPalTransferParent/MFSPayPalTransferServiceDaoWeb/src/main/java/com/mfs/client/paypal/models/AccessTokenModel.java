package com.mfs.client.paypal.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "access_token")
public class AccessTokenModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int accessTokenId;

	@Column(name = "token_expiration")
	private Date tokenExpiration;

	@Column(name = "token_value")
	private String tokenValue;

	@Column(name = "token_type")
	private String tokenType;

	@Column(name = "date_logged")
	private Date dateLogged;

	public int getAccessTokenId() {
		return accessTokenId;
	}

	public void setAccessTokenId(int accessTokenId) {
		this.accessTokenId = accessTokenId;
	}

	public Date getTokenExpiration() {
		return tokenExpiration;
	}

	public void setTokenExpiration(Date tokenExpiration) {
		this.tokenExpiration = tokenExpiration;
	}

	public String getTokenValue() {
		return tokenValue;
	}

	public void setTokenValue(String tokenValue) {
		this.tokenValue = tokenValue;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "AccessTokenModel [accessTokenId=" + accessTokenId + ", tokenExpiration=" + tokenExpiration
				+ ", tokenValue=" + tokenValue + ", tokenType=" + tokenType + ", dateLogged=" + dateLogged + "]";
	}

}
