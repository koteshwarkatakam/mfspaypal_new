package com.mfs.client.paypal.service;

import com.mfs.client.paypal.dto.AuthResponseDto;

public interface AuthService {
	
	public AuthResponseDto getAccessToken() ;

}
