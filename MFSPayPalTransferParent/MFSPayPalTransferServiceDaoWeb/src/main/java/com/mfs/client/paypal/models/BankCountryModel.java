package com.mfs.client.paypal.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bank_country")
public class BankCountryModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(name = "id")
	private int bankCountryId;
	@Column(name = "country_name")
	private String countryName;
	@Column(name = "country_code")
	private String countryCode;
	@Column(name = "status")
	private String status;
	
	public int getBankCountryId() {
		return bankCountryId;
	}
	public void setBankCountryId(int bankCountryId) {
		this.bankCountryId = bankCountryId;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "BankCountryModel [bankCountryId=" + bankCountryId + ", countryName=" + countryName + ", countryCode="
				+ countryCode + ", status=" + status + "]";
	}
	
	
	
}
