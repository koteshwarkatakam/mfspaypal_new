package com.mfs.client.paypal.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.mfs.client.paypal.dao.SystemConfigDao;
import com.mfs.client.paypal.dao.TransactionDao;
import com.mfs.client.paypal.dto.AcknowledgementArrayRequest;
import com.mfs.client.paypal.dto.AuthResponseDto;
import com.mfs.client.paypal.dto.ErrorArrayResponse;
import com.mfs.client.paypal.dto.ItemsAcknowledgementArrayresponseDto;
import com.mfs.client.paypal.dto.PayPalAcknowledgementRequestDto;
import com.mfs.client.paypal.dto.PayPalAcknowledgementResponseDto;
import com.mfs.client.paypal.exception.CommonException;
import com.mfs.client.paypal.exception.DaoException;
import com.mfs.client.paypal.models.TransferDetailsLogModel;
import com.mfs.client.paypal.service.AuthService;
import com.mfs.client.paypal.service.TransferAcknowledgementsService;
import com.mfs.client.paypal.util.CallServices;
import com.mfs.client.paypal.util.CommonConstant;
import com.mfs.client.paypal.util.HttpsConnectionRequest;
import com.mfs.client.paypal.util.HttpsConnectionResponse;
import com.mfs.client.paypal.util.JSONToObjectConversion;
import com.mfs.client.paypal.util.ResponseCodes;

@Service("TransferAcknowledgementsService")
public class TransferAcknowledgementsServiceImpl implements TransferAcknowledgementsService {

	private static final Logger LOGGER = LogManager.getLogger(TransferAcknowledgementsServiceImpl.class);

	@Autowired
	SystemConfigDao systemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	AuthService authService;

	public PayPalAcknowledgementResponseDto transferAcknowledgements() {

		PayPalAcknowledgementResponseDto response = null;
		PayPalAcknowledgementRequestDto request = new PayPalAcknowledgementRequestDto();
		List<AcknowledgementArrayRequest> list = new ArrayList<AcknowledgementArrayRequest>();
		Map<String, String> configMap = null;
		Gson gson = new Gson();

		String serviceResponse = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectionResponse httpsConResult = null;
		AuthResponseDto authResponse = new AuthResponseDto();

		try {

			authResponse = authService.getAccessToken();

			// getting data from config table
			configMap = systemConfigDao.getConfigDetailsMap();

			if (configMap == null) {

				response = new PayPalAcknowledgementResponseDto();
				response.setErrorCode(ResponseCodes.ER203.getCode());
				response.setErrorMessage(ResponseCodes.ER203.getMessage());

				return response;
			}

			// get all success and fail response from db
			List<TransferDetailsLogModel> getAllRecords = transactionDao.getAllSuccessAndFailRecord();

			for (TransferDetailsLogModel one : getAllRecords) {

				AcknowledgementArrayRequest request1 = new AcknowledgementArrayRequest();
				request1.setTracking_number(one.getTrackingNumber());
				request1.setCode(one.getRespCode());
				request1.setDescription(one.getMfsStatus());
				list.add(request1);

			}

			request.setAcknowledgements(list);

			// partner request
			String partnerRequest = gson.toJson(request);

			System.out.println(partnerRequest);

			String serviceSwitch = configMap.get(CommonConstant.SWITCH);
			if (serviceSwitch.equalsIgnoreCase("on")) {

				headerMap = new HashMap<String, String>();
				headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
				headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.TOKEN_TYPE + authResponse.getValue());
				connectionRequest.setHeaders(headerMap);

				// set service URL
				connectionRequest.setHttpmethodName("POST");
				if (configMap.get(CommonConstant.PORT) != null) {
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
				}
				connectionRequest.setServiceUrl(
						configMap.get(CommonConstant.BASE_URL) + configMap.get(CommonConstant.ACKNOWLEDGEMENT_ENDPOINT));

				// send request to partner
				httpsConResult = CallServices.getResponseFromService(connectionRequest, partnerRequest);

			} else {

				serviceResponse = "{\n" + "  \"total_items\": 3,\n" + "  \"total_pages\": 1,\n" + "  \"items\": [\n"
						+ "    {\n" + "      \"tracking_number\": \"X568214583\",\n" + "      \"error_code\": null,\n"
						+ "      \"error_message\": null\n" + "    },\n" + "    {\n"
						+ "      \"tracking_number\": \"X145625357\",\n" + "      \"error_code\": null,\n"
						+ "      \"error_message\": null\n" + "    },\n" + "    {\n"
						+ "      \"tracking_number\": \"X752565841\",\n" + "      \"error_code\": null,\n"
						+ "      \"error_message\": null\n" + "    }\n" + "  ],\n" + "  \"links\": {\n"
						+ "    \"href\": \"https://webservices.xoom.com/v2/handover/deposit/transfers/acknowledgements\",\n"
						+ "    \"rel\": \"self\",\n" + "    \"method\": \"POST\"\n" + "  },\n" + "  \"request\": {\n"
						+ "    \"id\": \"RID-7265688702352576\"\n" + "  }\n" + "}";

				httpsConResult = new HttpsConnectionResponse();
				httpsConResult.setRespCode(ResponseCodes.S200.getCode() + "");
				httpsConResult.setResponseData(serviceResponse);

			}

			// check null response

			if (httpsConResult != null) {
				serviceResponse = httpsConResult.getResponseData();
			}
			LOGGER.info("TransferAcknowledgmentServiceImpl in transferAcknowledgment function response : "
					+ serviceResponse);

			if (null == httpsConResult) {
				response = new PayPalAcknowledgementResponseDto();
				response.setErrorCode(ResponseCodes.NULL_RESPONSE.getCode());
				response.setErrorMessage(ResponseCodes.NULL_RESPONSE.getMessage());
			} else {

				if (httpsConResult.getRespCode().equals(ResponseCodes.S200.getCode() + "")) {
					response = (PayPalAcknowledgementResponseDto) JSONToObjectConversion
							.getObjectFromJson(serviceResponse, PayPalAcknowledgementResponseDto.class);
					logResponse(response);
				} else {
					response = new PayPalAcknowledgementResponseDto();
					response.setErrorCode(httpsConResult.getRespCode());
					response.setErrorMessage(httpsConResult.getTxMessage());

				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	private void logResponse(PayPalAcknowledgementResponseDto response) throws DaoException, CommonException {
		List<ErrorArrayResponse> errorResponse = null;

		if (response.getItems() != null) {
			List<ItemsAcknowledgementArrayresponseDto> responseList = response.getItems();

			for (ItemsAcknowledgementArrayresponseDto item : responseList) {

				TransferDetailsLogModel transferModel = transactionDao
						.getTransfersRecordBytrackingNumber(item.getTracking_number());

				if (transferModel != null) {

					if (item.getError_code() != null || item.getError_message() != null) {
						transferModel.setPaypalStatusCode(item.getError_code());
						transferModel.setPaypalStatus(item.getError_message());
					}
					
					transferModel.setDateLogged(new Date());
					transactionDao.update(transferModel);	
				}
				
			}
		}
	}

}
