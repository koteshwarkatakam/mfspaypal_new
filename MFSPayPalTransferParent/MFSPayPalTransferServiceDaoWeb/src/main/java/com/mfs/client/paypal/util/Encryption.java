package com.mfs.client.paypal.util;

import org.apache.commons.codec.binary.Base64;

public  class Encryption {

	public static String encryption(String username, String password) {

		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append(username);
		stringBuilder.append(":");
		stringBuilder.append(password);

		byte[] authEncBytes = Base64.encodeBase64(stringBuilder.toString().getBytes());
		String authStringEnc = new String(authEncBytes);
		return authStringEnc;

	}
	
	
}