package com.mfs.client.paypal.dto;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class RecipientDto {

	private String first_name;

	private String middle_name;

	private String last_name;

	private String maternal_last_name;

	private String phone;

	private String email;

	private RecipientAddressResponse address;

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getMaternal_last_name() {
		return maternal_last_name;
	}

	public void setMaternal_last_name(String maternal_last_name) {
		this.maternal_last_name = maternal_last_name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public RecipientAddressResponse getAddress() {
		return address;
	}

	public void setAddress(RecipientAddressResponse address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "RecipientDto [first_name=" + first_name + ", middle_name=" + middle_name + ", last_name=" + last_name
				+ ", maternal_last_name=" + maternal_last_name + ", phone=" + phone + ", email=" + email + ", address="
				+ address + "]";
	}

}
