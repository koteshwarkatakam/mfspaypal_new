package com.mfs.client.paypal.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class AcknowledgementArrayRequest {

	private String tracking_number;

	private String code;

	private String description;

	public String getTracking_number() {
		return tracking_number;
	}

	public void setTracking_number(String tracking_number) {
		this.tracking_number = tracking_number;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "AcknowledgementArrayRequest [tracking_number=" + tracking_number + ", code=" + code + ", description="
				+ description + "]";
	}

}
