package com.mfs.client.paypal.util;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class HttpsConnectionRequest {

	private String serviceUrl;

	private String httpmethodName;

	private int port;

	private Map<String, String> headers;

	public String getServiceUrl() {
		return serviceUrl;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	public String getHttpmethodName() {
		return httpmethodName;
	}

	public void setHttpmethodName(String httpmethodName) {
		this.httpmethodName = httpmethodName;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	@Override
	public String toString() {
		return "HttpsConnectionRequest [serviceUrl=" + serviceUrl + ", httpmethodName=" + httpmethodName + ", port="
				+ port + ", headers=" + headers + "]";
	}

}
