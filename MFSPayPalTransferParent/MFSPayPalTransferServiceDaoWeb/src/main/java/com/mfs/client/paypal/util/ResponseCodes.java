package com.mfs.client.paypal.util;

public enum ResponseCodes {

	S200("200", "OK"),

	ER202("ER202", "JazzCash Internal Error"), 
	ER204("ER204", "Error while saving data"),
	ER205("ER205", "Error while updating data"),
	VALIDATION_ERROR("ER206", "Validation Error"),
	ER207("ER207", "System Error"),
	ER212("ER212", "Error while getting record by Account Number"),
	ER213("ER213", "Null Value getting from DB for send transfer"), 
	ER215("ER215", "JazzCash API Null response"),
	ER235("ER235", "Error while getting record by MfsAfricBankName"),
	ER214("ER214", "Error while getting record by MfsTransId"), 
	ER209("ER209", "Transaction record not found"),
	ER208("ER208", "Error while getting last Record of EventLog"), 
	ER216("ER216", "MFSID not found"),
	ER220("ER220", "Error while connecting partner"),
	ER229("ER229", "Fail"),
	ER221("ER221", "No data found from System Config Table"),
	ER222("ER222", "Null Values getting from System Config i.e user or password or partnerId or transStatus"),
	ER223("ER223", "Null Values getting from System Config i.e user or password or partnerId"),
	ER230("ER230", "Bank details not found"),
	ER231("ER231", "Null Values getting from BanksList Table i.e Branch Code"),
	ER256("ER256", "Exception in SHA1 Hashing"), 
	ER257("ER257", "Exception occurred while creating soap request"),
	ER203("ER203", "Null system config details"),
	ER258("ER258", "Error while getting data from transfers details"),

	FORBIDDEN("FORBIDDEN", "Forbidden"), ENDPOINT_NOT_FOUND("ENDPOINT_NOT_FOUND", "Request Endpoint is not found"),
	INVALID_TOKEN("INVALID_TOKEN", "Token is not valid"), NULL_RESPONSE("NULL_RESPONSE", "null response"), 
	
	///////////MFS Response code
	ER270("ER270", "Error while getting records from transfer detail table"), 
	ER271("ER271", "Error while getting records from Scheduler table"),
	ER272("ER272", "Exception occurred in PayPalTransferSchedulerService"),
	ER201("ER201", "System Error"),
	
	LOG_SUCCESS("Log Success","Log Success"),
	LOG_FAIL("Log Fail" , "Log Fail"),
	COMMIT_QUEUED("Transaction Queued" , "Transaction Queued"),
	COMMIT_SUCCESS("Commit Success", "Commit Success"),
	COMMIT_PENDING("Commit Pending" , "Commit Pending"),
	COMMIT_FAIL("Commit Fail","Commit Fail");

	private String code;
	private String message;

	private ResponseCodes(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
