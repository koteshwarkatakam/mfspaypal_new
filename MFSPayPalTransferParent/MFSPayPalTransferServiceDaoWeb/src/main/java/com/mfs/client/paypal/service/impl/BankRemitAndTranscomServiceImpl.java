package com.mfs.client.paypal.service.impl;

import java.util.Date;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.dto.BankRemitLogRequestDto;
import com.mfs.client.dto.BankRemitLogResponseDto;
import com.mfs.client.dto.Login;
import com.mfs.client.dto.TransComRequestDto;
import com.mfs.client.dto.TransComResponseDto;
import com.mfs.client.paypal.dao.SystemConfigDao;
import com.mfs.client.paypal.dao.TransactionDao;
import com.mfs.client.paypal.dto.ItemsArrayResponseDto;
import com.mfs.client.paypal.exception.CommonException;
import com.mfs.client.paypal.exception.DaoException;
import com.mfs.client.paypal.models.TransferDetailsLogModel;
import com.mfs.client.paypal.service.BankRemitAndTranscomService;
import com.mfs.client.paypal.service.MFSBankRemitLogService;
import com.mfs.client.paypal.util.CommonConstant;
import com.mfs.client.paypal.util.MfsResponseCodes;
import com.mfs.client.paypal.util.ResponseCodes;
import com.mfs.client.service.BankRemitLogService;
import com.mfs.client.service.TransComService;
import com.mfs.client.service.impl.BankRemitLogServiceImpl;
@Service("BankRemitAndTranscomService")
public class BankRemitAndTranscomServiceImpl implements BankRemitAndTranscomService {
	
	@Autowired
	SystemConfigDao systemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	MFSBankRemitLogService mFSBankRemitRequest;

	@Autowired
	BankRemitLogService bankRemitLogService;

	@Autowired
	TransComService transComService;
	
	@Autowired
	BankRemitLogServiceImpl bankRemitLogServiceImpl;
	
	private static final Logger LOGGER = LogManager.getLogger(BankRemitAndTranscomServiceImpl.class);
	
	@SuppressWarnings("unused")
	public void bankRemitAndTranscomCall(ItemsArrayResponseDto itemsArray, Map<String, String> configMap,
			BankRemitLogRequestDto remmitRequest, int transRetryCount) throws CommonException, DaoException {
		// TODO Auto-generated method stub
		BankRemitLogResponseDto logResponse = null;
		TransComResponseDto transComResponse = null;
		String trackingNumber=null;
		// internal call to jar for calling bankRemitLog of
		// mfs api
	    	logResponse = bankRemitLogService.bankRemitLog(remmitRequest);
	    	
		LOGGER.info(
				"==>In BankRemitAndTranscomService of BankRemitAndTranscomServiceImpl function for remit_log response from MFSApiClient :: "
						+ logResponse);
		

		if (itemsArray.getTransfer().getTracking_number()!=null){
			trackingNumber=itemsArray.getTransfer().getTracking_number();
		}
		if((remmitRequest.getThird_party_trans_id()!=null)&&(!remmitRequest.getThird_party_trans_id().isEmpty())){
			trackingNumber=remmitRequest.getThird_party_trans_id();
		}
		// update remit log response
		updateRemitLogResponse(logResponse, trackingNumber, transRetryCount);

		/////// transcom process//////////
		if (logResponse!= null && logResponse.getReturnResponse() != null && logResponse.getReturnResponse().getMfs_trans_id() != null
				&& !logResponse.getReturnResponse().getMfs_trans_id().isEmpty()
				&& !logResponse.getReturnResponse().getMfs_trans_id().equals(CommonConstant.MFS_LOG_FAIL_TRANS_ID)) {

			String mfsTransId = logResponse.getReturnResponse().getMfs_trans_id();
			TransComRequestDto transComRequest = new TransComRequestDto();
			
			Login login = new Login();
			login.setCorporate_code(configMap.get(CommonConstant.CORPORATE_CODE));
			login.setPassword(configMap.get(CommonConstant.PASSWORD));
			transComRequest.setLogin(login);
			transComRequest.setTrans_id(mfsTransId);
			transComRequest.setWsdl(configMap.get(CommonConstant.WSDL));

			if (configMap.get(CommonConstant.X_API_KEY) != null) {
				transComRequest.setxApiKey(configMap.get(CommonConstant.X_API_KEY));
			}

			try {
				// internal call to jar for calling
				// trans_com of mfs api
				transComResponse = transComService.transComm(transComRequest);
			} catch (Exception e1) {
				LOGGER.error("Exception occured while commit process :: ", e1);
				updateTransComResponse(null, trackingNumber, transRetryCount);
				transComResponse = null;
			}
			LOGGER.info(
					"==>In BankRemitAndTranscomService of BankRemitAndTranscomServiceImpl function for trans_comm response from MFSApiClient "
							+ transComResponse);
			if (transComResponse != null) {

				// update trans com response
				updateTransComResponse(transComResponse, trackingNumber, transRetryCount);
			}

		}
		
		
	}

	private void updateRemitLogResponse(BankRemitLogResponseDto logResponse, String tracking_number, int transRetryCount) throws CommonException, DaoException {
		// TODO Auto-generated method stub
		TransferDetailsLogModel logModel = transactionDao.getTransfersRecordBytrackingNumber(tracking_number);
		if (logModel != null) {

			if(logResponse!=null){
				
			
			if (logResponse.getReturnResponse() != null) {

				if (logResponse.getReturnResponse().getMfs_trans_id() != null && !logResponse.getReturnResponse()
						.getMfs_trans_id().equals(CommonConstant.MFS_LOG_FAIL_TRANS_ID)) {

					logModel.setMfsTransId(logResponse.getReturnResponse().getMfs_trans_id());
					logModel.setMfsStatus(ResponseCodes.LOG_SUCCESS.getMessage());

				} else {

					logModel.setMfsStatus(ResponseCodes.LOG_FAIL.getMessage());

				}

			} else {

				if (logResponse.getCode().equalsIgnoreCase("0")) {

					if (transRetryCount == 2) {

						logModel.setMfsStatus(ResponseCodes.LOG_FAIL.getMessage());

					}else{
						logModel.setMfsStatus(ResponseCodes.COMMIT_QUEUED.getMessage());
					}

					logModel.setRetryCount(transRetryCount + 1);

				}else if(logResponse.getMessage().equalsIgnoreCase(ResponseCodes.ER201.getMessage())){
					logModel.setMfsStatus(ResponseCodes.COMMIT_QUEUED.getMessage());
					logModel.setRespCode(logResponse.getCode());
					logModel.setRespMessage(logResponse.getMessage());
					
				}  else {

					logModel.setMfsStatus(ResponseCodes.LOG_FAIL.getMessage());
					logModel.setRespCode(logResponse.getCode());
					logModel.setRespMessage(logResponse.getMessage());
				}

			}
			
		}else{
			logModel.setMfsStatus(ResponseCodes.COMMIT_QUEUED.getMessage());
		}
			logModel.setDateUpdated(new Date());
			transactionDao.update(logModel);
		}

	}

	private void updateTransComResponse(TransComResponseDto response2, String tracking_number, int count) throws DaoException, CommonException {

		TransferDetailsLogModel logModel = transactionDao.getTransfersRecordBytrackingNumber(tracking_number);

		if (logModel != null) {

			if (response2 == null) {

				logModel.setMfsStatus(ResponseCodes.COMMIT_PENDING.getMessage());
				logModel.setRespCode(MfsResponseCodes.ER201.getCode());
				logModel.setRespMessage(MfsResponseCodes.ER201.getMessage());
				logModel.setDateUpdated(new Date());
				transactionDao.update(logModel);

			} else {

				if (response2.getReturnResponse().getCode().equals(MfsResponseCodes.MR101.getCode())) {
					logModel.setMfsStatus(ResponseCodes.COMMIT_SUCCESS.getMessage());
				} else if (response2.getReturnResponse().getCode().equals(MfsResponseCodes.MR102.getCode())
						|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.MR103.getCode())
						|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.MR108.getCode())) {
					logModel.setMfsStatus(ResponseCodes.COMMIT_PENDING.getMessage());
				}

				
				 /* else if (serviceName != null) {
				  
				  if (response2.getReturnResponse().getCode().equals(
				  MfsResponseCodes.ER201.getCode())) {
				  
				  if (count == 2) {
				  
				  logModel.setMfsStatus(ResponseCodes.COMMIT_FAIL.getMessage());
				  
				  
				  } else if (response2.getReturnResponse().getCode().equals(
				  MfsResponseCodes.ER201.getCode())) {
				  
				  logModel.setMfsStatus(ResponseCodes.COMMIT_QUEUED.getMessage(
				  )); }
				  
				  logModel.setRetryCount(count + 1);
				  
				  }
				  
				  }*/
				
				
				  else if (response2.getReturnResponse().getCode().equals(MfsResponseCodes.ER201.getCode())
						|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.MR109.getCode())
						|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.ER108.getCode())
						|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.ER109.getCode())
						|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.ER110.getCode())
					/////// ********need to be verified once ER111=Commit fail
						//|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.ER111.getCode())
						|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.ER115.getCode())
						|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.ER200.getCode())
						|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.ER202.getCode())
						|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.ER204.getCode())
						|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.ER208.getCode())
						|| response2.getReturnResponse().getCode().equals(MfsResponseCodes.ER209.getCode())) {

					  if(logModel.getRetryCount()==2){
						  
						  logModel.setMfsStatus(ResponseCodes.COMMIT_FAIL.getMessage());
					      
					  }else{
					logModel.setMfsStatus(ResponseCodes.COMMIT_QUEUED.getMessage());
					
					  }
					  logModel.setRetryCount(logModel.getRetryCount() + 1);
				} else {
					logModel.setMfsStatus(ResponseCodes.COMMIT_FAIL.getMessage());
				}

				logModel.setRespCode(response2.getReturnResponse().getCode());
				logModel.setRespMessage(response2.getReturnResponse().getMessage());
				logModel.setDateUpdated(new Date());
				transactionDao.update(logModel);
			}
		}
	}

}
