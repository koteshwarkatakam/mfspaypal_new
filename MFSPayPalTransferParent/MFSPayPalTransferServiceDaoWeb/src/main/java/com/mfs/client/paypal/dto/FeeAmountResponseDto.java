package com.mfs.client.paypal.dto;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class FeeAmountResponseDto {

	private double amount;

	private String currency_code;

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency_code() {
		return currency_code;
	}

	public void setCurrency_code(String currency_code) {
		this.currency_code = currency_code;
	}

	@Override
	public String toString() {
		return "FeeAmountResponseDto [amount=" + amount + ", currency_code=" + currency_code + "]";
	}

}
