package com.mfs.client.paypal.dto;

import java.util.List;

public class PayPalAcknowledgementRequestDto {

	private List<AcknowledgementArrayRequest> acknowledgements;

	public List<AcknowledgementArrayRequest> getAcknowledgements() {
		return acknowledgements;
	}

	public void setAcknowledgements(List<AcknowledgementArrayRequest> acknowledgements) {
		this.acknowledgements = acknowledgements;
	}

	@Override
	public String toString() {
		return "PayPalAcknowledgementRequestDto [acknowledgements=" + acknowledgements + "]";
	}

}
