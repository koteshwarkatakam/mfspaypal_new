package com.mfs.client.paypal.service.impl;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.dto.BankRemitLogRequestDto;
import com.mfs.client.paypal.dao.SystemConfigDao;
import com.mfs.client.paypal.dao.TransactionDao;
import com.mfs.client.paypal.dto.AuthResponseDto;
import com.mfs.client.paypal.dto.ItemsArrayResponseDto;
import com.mfs.client.paypal.dto.PayPalTransferResponseDto;
import com.mfs.client.paypal.exception.CommonException;
import com.mfs.client.paypal.exception.DaoException;
import com.mfs.client.paypal.models.TransferDetailsLogModel;
import com.mfs.client.paypal.service.AuthService;
import com.mfs.client.paypal.service.BankRemitAndTranscomService;
import com.mfs.client.paypal.service.MFSBankRemitLogService;
import com.mfs.client.paypal.service.PayPalTransferService;
import com.mfs.client.paypal.util.CallServices;
import com.mfs.client.paypal.util.CommonConstant;
import com.mfs.client.paypal.util.HttpsConnectionRequest;
import com.mfs.client.paypal.util.HttpsConnectionResponse;
import com.mfs.client.paypal.util.JSONToObjectConversion;
import com.mfs.client.paypal.util.ResponseCodes;

@Service("PayPalTransferService")
public class PayPalTransferServiceImpl implements PayPalTransferService {

	private static final Logger LOGGER = LogManager.getLogger(PayPalTransferServiceImpl.class);

	@Autowired
	SystemConfigDao systemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	AuthService authService;

	@Autowired
	MFSBankRemitLogService mFSBankRemitRequest;

	/*@Autowired
	@Qualifier("poolTargetSourceThreadUtil")
	CommonsPool2TargetSource poolTargetSourceThreadUtil;*/

	@Autowired
	BankRemitAndTranscomService bankRemitAndTranscomService;

	public PayPalTransferResponseDto getDepositTransfers() {
		System.out.println(" in getDepositTransfers");
		PayPalTransferResponseDto response = new PayPalTransferResponseDto();
		Map<String, String> configMap = null;
		HttpsConnectionResponse httpsConResult = null;
		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		String getTransfersRequest = null;
		String serviceResponse = null;
		AuthResponseDto authResponse = new AuthResponseDto();
		BankRemitLogRequestDto remmitRequest = new BankRemitLogRequestDto();
		
		try {
			// system config details
			configMap = systemConfigDao.getConfigDetailsMap();
			authResponse = authService.getAccessToken();
			String serviceSwitch = configMap.get(CommonConstant.SWITCH);
			if (serviceSwitch.equalsIgnoreCase("on")) {
				// set header
				headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.APPLICATION_JSON);
				headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.TOKEN_TYPE + authResponse.getValue());
				connectionRequest.setHeaders(headerMap);

				// set service url
				connectionRequest.setHttpmethodName("POST");

				if (configMap.get(CommonConstant.PORT) != null) {
					connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
				}

				connectionRequest
						.setServiceUrl(configMap.get(CommonConstant.BASE_URL) +configMap.get(CommonConstant.GET_TRANSFERS_ENDPOINT)
						+"?batch_size="+configMap.get(CommonConstant.BATCH_SIZE));

				LOGGER.info("PayPalTransferServiceImpl in PayPalTransferService function sending request "
						+ connectionRequest.getServiceUrl() + " Headers " + CommonConstant.CONTENT_TYPE + ":"
						+ CommonConstant.APPLICATION_JSON + "  " + CommonConstant.AUTHORIZATION + ":"
						+ CommonConstant.TOKEN_TYPE + authResponse.getValue());

				httpsConResult = CallServices.getResponseFromService(connectionRequest, getTransfersRequest);
			} else {

				serviceResponse = "{\n  \"total_items\": 2,\n  \"total_pages\": 1,\n  \"items\": [\n    {\n      \"transfer\": {\n        \"account_number\": \"00132509885\",\n        \"account_name\": \"Pedro Alberto Molina Theissen\",\n        \"account_type\": \"SAVINGS\",\n        \"routing_number\": \"33291\",\n        \"bank_code\": \"0242\",\n        \"bank_name\": \"Banco de Comercio\",\n        \"branch_code\": \"55454\",\n        \"branch_name\": \"Central\",\n        \"branch_district\": \"Central\",\n        \"sub_bank_code\": \"12312\",\n        \"sub_bank_name\": \"Central\",\n        \"tracking_number\": \"XC09782346\",\n        \"created\": \"2021-09-14T23:23:08.000Z\",\n        \"send_amount\": {\n          \"amount\": 98.23,\n          \"currency_code\": \"USD\"\n        },\n        \"receive_amount\": {\n          \"amount\": 789.12,\n          \"currency_code\": \"GTQ\"\n        },\n        \"fee_amount\": {\n          \"amount\": 4.99,\n          \"currency_code\": \"USD\"\n        }\n      },\n      \"recipient\": {\n        \"first_name\": \"Pedro\",\n        \"middle_name\": \"Alberto\",\n        \"last_name\": \"Molina\",\n        \"maternal_last_name\": \"Theissen\",\n        \"phone\": \"41524252714\",\n        \"email\": \"pmolina@email.com\",\n        \"address\": {\n          \"address1\": \"24 av. 3-23 Zona 7\",\n          \"address2\": \"Prados del Rodeo\",\n          \"city\": \"Guatemala\",\n          \"county\": \"Guatemala\",\n          \"state\": \"Guatemala\",\n          \"postal_code\": \"01007\",\n          \"country\": \"GT\"\n        }\n      },\n      \"sender\": {\n        \"first_name\": \"James\",\n        \"middle_name\": \"T.\",\n        \"last_name\": \"Stevens\",\n        \"maternal_last_name\": \"McAllister\",\n        \"phone\": \"+13342009999\",\n        \"email\": \"jstevens@email.com\",\n        \"address\": {\n          \"address1\": \"1514 Main St\",\n          \"address2\": \"St. Ange Building\",\n          \"city\": \"Dothan\",\n          \"county\": \"Houston\",\n          \"state\": \"Alabama\",\n          \"postal_code\": \"36301\",\n          \"country\": \"US\"\n        }\n      },\n      \"additional_fields\": [\n        {\n          \"name\": \"SENDER_FINANCIAL_INSTITUTION_ID\",\n          \"value\": \"121042882\"\n        }\n      ]\n    },\n    {\n      \"transfer\": {\n        \"account_number\": \"00132509885\",\n        \"account_name\": \"Pedro Alberto Molina Theissen\",\n        \"account_type\": \"SAVINGS\",\n        \"routing_number\": \"33291\",\n        \"bank_code\": \"0242\",\n        \"bank_name\": \"Banco de Comercio\",\n        \"branch_code\": \"55454\",\n        \"branch_name\": \"Central\",\n        \"branch_district\": \"Central\",\n        \"sub_bank_code\": \"12312\",\n        \"sub_bank_name\": \"Central\",\n        \"tracking_number\": \"XB09782345\",\n        \"created\": \"2021-09-14T23:23:07.000Z\",\n        \"send_amount\": {\n          \"amount\": 98.23,\n          \"currency_code\": \"USD\"\n        },\n        \"receive_amount\": {\n          \"amount\": 789.12,\n          \"currency_code\": \"GTQ\"\n        },\n        \"fee_amount\": {\n          \"amount\": 4.99,\n          \"currency_code\": \"USD\"\n        }\n      },\n      \"recipient\": {\n        \"first_name\": \"Pedro\",\n        \"middle_name\": \"Alberto\",\n        \"last_name\": \"Molina\",\n        \"maternal_last_name\": \"Theissen\",\n        \"phone\": \"41524252714\",\n        \"email\": \"pmolina@email.com\",\n        \"address\": {\n          \"address1\": \"24 av. 3-23 Zona 7\",\n          \"address2\": \"Prados del Rodeo\",\n          \"city\": \"Guatemala\",\n          \"county\": \"Guatemala\",\n          \"state\": \"Guatemala\",\n          \"postal_code\": \"01007\",\n          \"country\": \"GT\"\n        }\n      },\n      \"sender\": {\n        \"first_name\": \"James\",\n        \"middle_name\": \"T.\",\n        \"last_name\": \"Stevens\",\n        \"maternal_last_name\": \"McAllister\",\n        \"phone\": \"+13342009999\",\n        \"email\": \"jstevens@email.com\",\n        \"address\": {\n          \"address1\": \"1514 Main St\",\n          \"address2\": \"St. Ange Building\",\n          \"city\": \"Dothan\",\n          \"county\": \"Houston\",\n          \"state\": \"Alabama\",\n          \"postal_code\": \"36301\",\n          \"country\": \"US\"\n        }\n      },\n      \"additional_fields\": [\n        {\n          \"name\": \"SENDER_FINANCIAL_INSTITUTION_ID\",\n          \"value\": \"121042882\"\n        }\n      ]\n    }\n  ],\n  \"links\": {\n    \"href\": \"https://webservices.xoom.com/v2/handover/deposit/transfers?batch_size=2\",\n    \"rel\": \"first\",\n    \"method\": \"GET\"\n  },\n  \"request\": {\n    \"id\": \"RID-3255130480974235\"\n  }\n}";
				httpsConResult = new HttpsConnectionResponse();
				httpsConResult.setRespCode(ResponseCodes.S200.getCode() + "");
				httpsConResult.setResponseData(serviceResponse);
			}
			if (httpsConResult != null) {
				serviceResponse = httpsConResult.getResponseData();
			}

			LOGGER.info("PayPalTransferServiceImpl in PayPalTransferService function receiving response from PayPal: "
					+ serviceResponse);

			if (null == httpsConResult) {
				response = new PayPalTransferResponseDto();
				response.setErrorCode(CommonConstant.PAYPAL_NULL_RESPONSE);
				response.setErrorMessage(CommonConstant.PAYPAL_NULL_RESPONSE_CODE);

			} else {

				if (httpsConResult.getRespCode().equalsIgnoreCase(CommonConstant.SUCCESS_CODE)) {

					if (serviceResponse != null) {

						response = (PayPalTransferResponseDto) JSONToObjectConversion.getObjectFromJson(serviceResponse,
								PayPalTransferResponseDto.class);

						// log response

						// to get mfs api remmit request
						for (ItemsArrayResponseDto itemsArray : response.getItems()) {

							// Start of Thread Pool Code added by TechJivaa
							
							/*ThreadUtil threadUtil = (ThreadUtil) poolTargetSourceThreadUtil.getTarget();
							LOGGER.info("Object Hashcode:" + threadUtil.hashCode());

							worldRemitResponse = threadUtil.cancelTransactionThread(correspondentVal);
							poolTargetSourceThreadUtil.releaseTarget(threadUtil);*/
							// End of Thread Pool Code added by TechJivaa Date:
							
							TransferDetailsLogModel transfersModel = transactionDao
									.getTransfersRecordBytrackingNumber(itemsArray.getTransfer().getTracking_number());

							if (transfersModel == null) {

								// log the response in DB
								logResponse(itemsArray);
								// preparing remitt request
								remmitRequest = mFSBankRemitRequest.remmitRequest(itemsArray, configMap);
								int retryCount = 0;
								
								
								// calling Bankremit and transcom function
								bankRemitAndTranscomService.bankRemitAndTranscomCall(itemsArray, configMap,
										remmitRequest, retryCount);
							} else {
								LOGGER.info(
										"PayPalTransferServiceImpl in PayPalTransferService function tracking number already present in DB=> "
												+ itemsArray.getTransfer().getTracking_number());
							}
						}
					} else {
						response = new PayPalTransferResponseDto();
						response.setErrorCode(httpsConResult.getRespCode());
						response.setErrorMessage(httpsConResult.getTxMessage());
						// logResponse(response);
					}
				} else {
					response = new PayPalTransferResponseDto();
					response.setErrorCode(httpsConResult.getRespCode());
					response.setErrorMessage(httpsConResult.getTxMessage());
					// logResponse(response);
				}

			}
		} catch (CommonException ce) {
			LOGGER.error("==>CommonException in PayPalTransferService of PayPalTransferServiceImpl", ce);
		} catch (Exception e) {
			LOGGER.error("==>Exception in MFSApiClient of RemitLogServiceImpl", e);
		}
		return response;

	}

	private void logResponse(ItemsArrayResponseDto itemsArray) throws CommonException, DaoException {

		TransferDetailsLogModel transfersModel = new TransferDetailsLogModel();

		transfersModel.setAccountNumber(itemsArray.getTransfer().getAccount_number());
		transfersModel.setAccountName(itemsArray.getTransfer().getAccount_name());
		transfersModel.setAccountType(itemsArray.getTransfer().getAccount_type());
		transfersModel.setBankCode(itemsArray.getTransfer().getBank_code());
		transfersModel.setBankName(itemsArray.getTransfer().getBank_name());
		transfersModel.setTrackingNumber(itemsArray.getTransfer().getTracking_number());
		transfersModel.setRoutingNummber(itemsArray.getTransfer().getRouting_number());
		transfersModel.setSendAmount(String.valueOf(itemsArray.getTransfer().getSend_amount().getAmount()));
		transfersModel.setSendCurCode(itemsArray.getTransfer().getSend_amount().getCurrency_code());
		transfersModel.setFeeAmount(String.valueOf(itemsArray.getTransfer().getFee_amount().getAmount()));
		transfersModel.setFeeCurCode(itemsArray.getTransfer().getFee_amount().getCurrency_code());
		transfersModel.setReceiveAmount(String.valueOf(itemsArray.getTransfer().getReceive_amount().getAmount()));
		transfersModel.setRecCurCode(itemsArray.getTransfer().getReceive_amount().getCurrency_code());
		transfersModel.setReceiverFirstName(itemsArray.getRecipient().getFirst_name());
		transfersModel.setReceiverMiddleName(itemsArray.getRecipient().getMiddle_name());
		transfersModel.setReceiverLastName(itemsArray.getRecipient().getLast_name());
		transfersModel.setReceiverMsisdn(itemsArray.getRecipient().getPhone());
		transfersModel.setReceiverCountry(itemsArray.getRecipient().getAddress().getCountry());
		transfersModel.setSenderFirstName(itemsArray.getSender().getFirst_name());
		transfersModel.setSenderLastName(itemsArray.getSender().getLast_name());
		transfersModel.setSenderCountry(itemsArray.getSender().getAddress().getCountry());
		transfersModel.setRetryCount(0);
		transfersModel.setPaypalStatus(ResponseCodes.COMMIT_QUEUED.getMessage());
		transfersModel.setPaypalStatusCode("");

		// MFS optional fields
		transfersModel.setSenderAddress(itemsArray.getSender().getAddress().getAddress1());
		transfersModel.setSenderCity(itemsArray.getSender().getAddress().getCity());
		transfersModel.setSenderEmail(itemsArray.getSender().getEmail());
		transfersModel.setSenderMsisdn(itemsArray.getSender().getPhone());
		transfersModel.setSenderPostalCode(itemsArray.getSender().getAddress().getPostal_code());
		transfersModel.setSenderState(itemsArray.getSender().getAddress().getState());
		transfersModel.setReceiverAddress(itemsArray.getRecipient().getAddress().getAddress1());
		transfersModel.setReceiverCity(itemsArray.getRecipient().getAddress().getCity());
		transfersModel.setReceiverEmail(itemsArray.getRecipient().getEmail());
		transfersModel.setReceiverPostalCode(itemsArray.getRecipient().getAddress().getPostal_code());
		transfersModel.setReceiverState(itemsArray.getRecipient().getAddress().getState());
		//////////////////////////////////
		transfersModel.setDateLogged(new Date());
		transfersModel.setDateUpdated(new Date());
		transactionDao.save(transfersModel);

	}

}
