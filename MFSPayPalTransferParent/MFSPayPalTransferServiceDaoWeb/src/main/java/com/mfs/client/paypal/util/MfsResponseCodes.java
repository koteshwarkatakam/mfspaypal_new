package com.mfs.client.paypal.util;

public enum MfsResponseCodes {

	
	MR101("MR101","Success"),
	MR102("MR102","Pending"),
	MR103("MR103","Pending"),
	FAIL("00000000", "Fail"),
	
	MR104("MR104","Log Success"),
	MR105("MR105","Cash pick up complete"),
	MR106("MR106","Cash pick up pending"),
	MR107("MR107","Cash pick up cancelled"),
	MR108("MR108","Transaction Queued"),
	MR109("MR109","Transaction cancelled"),

	ER101("ER101", "Authentication error"),
	ER102("ER102", "Invalid partner code"),
	ER103("ER103", "Partner corridor not active"),
	ER104("ER104", "The currency code provided is not ISO 4217 compliant"),
	ER105("ER105", "Invalid amount format"),
	ER106("ER106", "Invalid Account Number"),
	ER107("ER107", "Other Reject / Other Reject"),
	ER108("ER108", "Invalid Account / Account Unavailable for Deposit"),
	ER109("ER109", "Subscriber not authorized to receive amount"),
	ER110("ER110", "Insufficient fund in merchant account"),
	ER111("ER111", "Transaction	could not be executed"),
	ER115("ER115", "The transaction proposal has expired (more than 24 hours)"),
	ER200("ER200", "Payer Unavailable / E-Wallet System error"),
	ER201("ER201", "Generic reject - MFS System error"),
	ER202("ER202", "Black List, Compliance Reject"),
	ER203("ER203", "Transaction Velocity Exceeded - Daily Sender Velocity Limit Exceeded"),
	ER204("ER204", "Transaction Velocity Exceeded - Daily Recipient Velocity Limit Exceeded"),
	ER205("ER205", "Transaction Velocity Exceeded - Weekly Sender Velocity Limit Exceeded"),
	ER206("ER206", "Transaction Velocity Exceeded - Weekly Recipient Velocity Limit Exceeded"),
	ER207("ER207", "Transaction Velocity Exceeded - Monthly Sender Velocity Limit Exceeded"),
	ER208("ER208", "Transaction Velocity Exceeded - Monthly Recipient Velocity Limit Exceeded"),
	ER209("ER209", "Transaction Velocity Exceeded -Transaction Max Amount exceeded"),
	ER0014("ER0014", "Transaction Velocity Exceeded - Daily Transction Count Exceeded"),
	ER210("ER210","Bank account not found");
	private String code;
	private String message;
	
	public String getCode() {
		return code;
	}
	
	public String getMessage() {
		return message;
	}

	private MfsResponseCodes(String code, String message) {
		this.code = code;
		this.message = message;
	}

	
}
