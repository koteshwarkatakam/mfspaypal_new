package com.mfs.client.paypal.dao;

import java.util.Map;

import com.mfs.client.paypal.exception.CommonException;

public interface SystemConfigDao {

	public Map<String, String> getConfigDetailsMap() throws CommonException;

}
