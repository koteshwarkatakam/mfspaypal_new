package com.mfs.client.paypal.dto;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class ItemsAcknowledgementArrayresponseDto {

	private String tracking_number;

	private String error_code;

	private String error_message;

	public String getTracking_number() {
		return tracking_number;
	}

	public void setTracking_number(String tracking_number) {
		this.tracking_number = tracking_number;
	}

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}

	@Override
	public String toString() {
		return "ItemsAcknowledgementArrayresponseDto [tracking_number=" + tracking_number + ", error_code=" + error_code
				+ ", error_message=" + error_message + "]";
	}

}
