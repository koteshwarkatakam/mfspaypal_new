/*package com.mfs.client.paypal.service.thread;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

public class ThreadUtil {
	final static Logger LOGGER = Logger.getLogger(ThreadUtil.class);

	String threadName;

	@Autowired
	private NotifyTransAvailService notifyTransAvailService;

	public ThreadUtil() {
		LOGGER.info(" Object Created:" + this.hashCode());
	}

	public String getThreadName() {
		return threadName;
	}

	public void initialize(String threadName) {
		this.threadName = threadName;
	}

	public WorldRemitResponse transactionStartThread(String correspondent) {

		LOGGER.info(getThreadName() + " of TransactionStartThread is running");
		WorldRemitResponse worldRemitResponse = null;
		try {

			LOGGER.info(getThreadName() + " processing for transactionStartThread -> " + correspondent);

			if (correspondent != null) {
				worldRemitResponse = notifyTransAvailService.transactionStart(correspondent);
				LOGGER.debug(worldRemitResponse);
			}

			LOGGER.info(getThreadName() + " Execution completed.");

		} catch (Exception e) {
			LOGGER.error(e);
		}
		return worldRemitResponse;
	}

	public WorldRemitResponse transactionStatusGetThread(String correspondent) {

		LOGGER.info(getThreadName() + " of TransactionStatusGetThread is running");
		WorldRemitResponse worldRemitResponse = null;
		try {
			LOGGER.info(getThreadName() + "processing for transactionStatusGetThread -> " + correspondent);
			if (correspondent != null) {
				worldRemitResponse = notifyTransAvailService.transactionStatusGet(correspondent);
				LOGGER.debug(worldRemitResponse);
			}
			LOGGER.info(getThreadName() + " Execution completed.");

		} catch (Exception e) {
			LOGGER.error(e);
		}
		return worldRemitResponse;
	}

	public WorldRemitResponse cancelTransactionThread(String correspondent) {

		LOGGER.info(getThreadName() + " of CancelTransactionThread is running");
		WorldRemitResponse worldRemitResponse = null;
		try {

			LOGGER.info(getThreadName() + "processing for cancelTransactionThread -> " + correspondent);
			if (correspondent != null) {
				worldRemitResponse = notifyTransAvailService.cancelTransaction(correspondent);
				LOGGER.debug(worldRemitResponse);
			}

			LOGGER.info(getThreadName() + " Execution completed.");

		} catch (Exception e) {
			LOGGER.error(e);
		}
		return worldRemitResponse;
	}
}
*/