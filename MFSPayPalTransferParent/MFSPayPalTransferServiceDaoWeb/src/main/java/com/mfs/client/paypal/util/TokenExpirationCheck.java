package com.mfs.client.paypal.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.mfs.client.paypal.exception.DaoException;
import com.mfs.client.paypal.models.AccessTokenModel;

public class TokenExpirationCheck {

	/*@Autowired
	TransactionDao transactionDao;*/

	public static int invalidSession(AccessTokenModel authModel) throws DaoException, ParseException

	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d = authModel.getDateLogged();
		String string1 = format.format(d);
		Date d5 = format.parse(string1);
		Date d1 = new Date();
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		cal.setTime(d1);
		cal1.setTime(d);
		int day = cal.get(Calendar.DATE);
		int day1 = cal1.get(Calendar.DATE);
		int diff = (int) (d1.getTime() - d5.getTime());
		int diffMinutes = diff / (60 * 1000) % 60;
		int diffHours1 = diff / (60 * 60 * 1000) % 24;
		int minutes = Math.abs(diffMinutes);
		int minutes1 = concat(diffHours1, minutes);

		if (day == day1 && minutes1 <= 59) {

			int z = concat(diffHours1, minutes);
			return z;
		}
		return 62;
	}

	private static int concat(int diffHours1, int minutes) {
		String s1 = Integer.toString(diffHours1);
		String s2 = Integer.toString(minutes);

		// Concatenate both strings
		String s = s1 + s2;

		// Convert the concatenated string
		// to integer
		int c = Integer.parseInt(s);

		// return the formed integer
		return c;
	}
}
