package com.mfs.client.paypal.service.impl;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.paypal.dao.TransactionDao;
import com.mfs.client.paypal.models.SchedulerLogModel;
import com.mfs.client.paypal.service.PayPalTransferSchedulerService;
import com.mfs.client.paypal.service.PayPalTransferService;
import com.mfs.client.paypal.service.TransactionQueuedService;
import com.mfs.client.paypal.service.TransferAcknowledgementsService;
import com.mfs.client.paypal.util.CommonConstant;

@Service("PayPalTransferSchedulerService")
public class PayPalTransferSchedulerServiceImpl implements PayPalTransferSchedulerService {
	private static final Logger log = LogManager.getLogger(PayPalTransferSchedulerServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	PayPalTransferService payPalTransferService;

	@Autowired
	TransactionQueuedService transactionQueuedService;
	
	@Autowired
	TransferAcknowledgementsService transferAcknowledgementsService;

	@SuppressWarnings("unused")
	@Override
	public String schedule(String schedulerType) throws Exception {
		SchedulerLogModel schedularStatus = null;
		try {
			schedularStatus = transactionDao.getTransferScheduleStatus(schedulerType);
			System.out.println("scheduler status : " + schedularStatus.getStatus());
			if (schedularStatus != null) {
				if (!schedularStatus.getStatus().equalsIgnoreCase(CommonConstant.SCHEDULER_ACTIVE)) {

					schedularStatus.setStatus(CommonConstant.SCHEDULER_ACTIVE);
					schedularStatus.setExecTimestamp(new Date());
					transactionDao.update(schedularStatus);

					System.out.println("scheduler start and updated status as active");
					// get Transfers service call-call to paypal
					if (schedulerType.equalsIgnoreCase(CommonConstant.GETTRANSFERS_SCHED_TYPE)) {
						System.out.println("calling getdeposit transfers service");
						payPalTransferService.getDepositTransfers();
					} else if (schedulerType.equalsIgnoreCase(CommonConstant.QUEUED_TRANS_SCHED_TYPE)) {
						System.out.println("calling Queued transaction service");
						transactionQueuedService.queuedTransactionProcess();
					}
					else if (schedulerType.equalsIgnoreCase(CommonConstant.PENDING_ACKNOW_SCHED_TYPE)) {
						System.out.println("calling get pending acknowledgement service");
						transferAcknowledgementsService.transferAcknowledgements();
					}
					else if (schedulerType.equalsIgnoreCase(CommonConstant.GETBANKS_SCHED_TYPE)) {
						System.out.println("calling get banks service");
						transferAcknowledgementsService.transferAcknowledgements();
					}
					

					schedularStatus.setStatus(CommonConstant.SCHEDULER_INACTIVE);
					schedularStatus.setExecTimestamp(new Date());
					transactionDao.update(schedularStatus);
					System.out.println("get transfers service done updated scheduler");
				} else {

					System.out.println("scheduler is active already running");
					log.info("scheduler is active already running");
					schedularStatus.setStatus(CommonConstant.SCHEDULER_ACTIVE);
					schedularStatus.setExecTimestamp(new Date());
					transactionDao.update(schedularStatus);
				}
			} else {
				System.out.println("null scheduler data found in table");
				log.info("Transfer Scheduler data not available");
				return "Scheduler data not available";
			}

		} catch (Exception e) {
			log.error("==>Exception thrown in PayPalTransferSchedulerService", e);
			/*
			 * String status = new ResponseStatus();
			 * status.setStatusCode(ResponseCodes.ER272.getCode());
			 * status.setStatusMessage(ResponseCodes.ER272.getMessage());
			 */
			throw new Exception(e.getMessage());
		}
		return "ok";
	}

}
