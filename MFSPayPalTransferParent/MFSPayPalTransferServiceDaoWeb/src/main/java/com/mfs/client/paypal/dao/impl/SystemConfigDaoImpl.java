package com.mfs.client.paypal.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.paypal.dao.SystemConfigDao;
import com.mfs.client.paypal.dto.ResponseStatus;
import com.mfs.client.paypal.exception.CommonException;
import com.mfs.client.paypal.models.SystemConfigModel;
import com.mfs.client.paypal.util.ResponseCodes;

@EnableTransactionManagement
@Repository("SystemConfigDao")
public class SystemConfigDaoImpl implements SystemConfigDao {

	//private static final Logger LOGGER = Logger.getLogger(SystemConfigDaoImpl.class);
	private static Logger LOGGER = LogManager.getLogger(SystemConfigDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public Map<String, String> getConfigDetailsMap() throws CommonException {
		Map<String, String> systemConfigMap = null;

		try {
			Session session = sessionFactory.getCurrentSession();
			String hql = "From SystemConfigModel";
			Query query = session.createQuery(hql);

			List<SystemConfigModel> systemConfig = query.list();

			if (systemConfig != null && !systemConfig.isEmpty()) {
				systemConfigMap = new HashMap<String, String>();
				for (SystemConfigModel systemConfiguration : systemConfig) {
					systemConfigMap.put(systemConfiguration.getConfigKey(), systemConfiguration.getConfigValue());
				}
			}
		} catch (Exception e) {
			LOGGER.error("==>CommonException in SystemConfigDaoImpl" , e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER213.getMessage());
			status.setStatusCode(ResponseCodes.ER213.getCode());
			throw new CommonException(status);
		}
		return systemConfigMap;
	}

}
