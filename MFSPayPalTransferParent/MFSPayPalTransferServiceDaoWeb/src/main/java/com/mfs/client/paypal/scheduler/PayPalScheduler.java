package com.mfs.client.paypal.scheduler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;

import com.mfs.client.paypal.dao.SystemConfigDao;
import com.mfs.client.paypal.dao.TransactionDao;
import com.mfs.client.paypal.service.PayPalTransferSchedulerService;

@EnableScheduling
@Configuration
public class PayPalScheduler implements SchedulingConfigurer {

	@Autowired
	PayPalTransferSchedulerService payPalTransferSchedulerService;

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDao systemConfigDao;

	private static final Logger LOGGER = LogManager.getLogger(PayPalScheduler.class);

	private static Boolean isJobRunning = false;

	/*
	 * @Override public void configureTasks(ScheduledTaskRegistrar
	 * scheduledTaskRegistrar) { Runnable runnable = () -> System.out.println(
	 * "Trigger task executed at " + new Date());
	 * 
	 * Trigger trigger = new Trigger() {
	 * 
	 * @Override
	 * 
	 * public Date nextExecutionTime(TriggerContext triggerContext) {
	 * 
	 * CronTrigger crontrigger = new CronTrigger("0 0/1 * * * *");
	 * System.out.println("0 0/1 * * * *"); return
	 * crontrigger.nextExecutionTime(triggerContext);
	 * 
	 * }
	 * 
	 * };
	 * 
	 * scheduledTaskRegistrar.addTriggerTask(runnable, trigger);
	 * 
	 * }
	 */
	@Override
	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {

		// Scheduler for getTransfers
		taskRegistrar.addTriggerTask(() -> {

			try {
				
				System.out.println("calling scheduler service impl ");
				payPalTransferSchedulerService.schedule("getTransfers");
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("Exception generated at getTransfers scheduler : " + e.getMessage());
			}
		}, triggerContext -> {
			String cron = "";
			try {
				cron = systemConfigDao.getConfigDetailsMap().get("get_transfers_cron");
				// cron = "0 */2 * * * *";
				System.out.println(cron);
			} catch (Exception e) {
				LOGGER.error("Exception generated at gettransfers scheduler while getting CRON expression from DB : "
						+ e.getMessage());
			}
			return new CronTrigger(cron).nextExecutionTime(triggerContext);
		});

		// Scheduler for Queued Transactions :> call to Adapter to MFS
		taskRegistrar.addTriggerTask(() -> {

			try {

				System.out.println("calling scheduler service impl ");
				payPalTransferSchedulerService.schedule("getTransfers");
			} catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("Exception generated at getTransfers scheduler : " + e.getMessage());
			}
		}, triggerContext -> {
			String cron = "";
			try {
				cron = systemConfigDao.getConfigDetailsMap().get("get_transfers_cron");
				// cron = "0 */2 * * * *";
				System.out.println(cron);
			} catch (Exception e) {
				LOGGER.error("Exception generated at gettransfers scheduler while getting CRON expression from DB : "
						+ e.getMessage());
			}
			return new CronTrigger(cron).nextExecutionTime(triggerContext);
		});

	}
}