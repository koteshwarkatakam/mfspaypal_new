package com.mfs.client.paypal.service;

import com.mfs.client.paypal.dto.PayPalTransferResponseDto;

public interface PayPalTransferService {

	PayPalTransferResponseDto getDepositTransfers();


}
