package com.mfs.client.paypal.dao;

import java.util.List;

import com.mfs.client.paypal.exception.DaoException;
import com.mfs.client.paypal.models.AccessTokenModel;
import com.mfs.client.paypal.models.BankCodeModel;
import com.mfs.client.paypal.models.BankCountryModel;
import com.mfs.client.paypal.models.SchedulerLogModel;
import com.mfs.client.paypal.models.TransferDetailsLogModel;

public interface TransactionDao extends BaseDao {

	public AccessTokenModel getAuthTokenDetailsByLastRecord() throws DaoException;

	public TransferDetailsLogModel getTransfersRecordBytrackingNumber(String tracking_number)throws DaoException;

	public List<TransferDetailsLogModel> getAllQueuedTransaction(int count)throws DaoException;
	
	public List<TransferDetailsLogModel> getAllSuccessAndFailRecord() throws DaoException;

	public List<TransferDetailsLogModel> getPendingTransaction(final int count) throws DaoException;

	public TransferDetailsLogModel getTransferDetails(String mfsTransId) throws DaoException;

	public SchedulerLogModel getTransferScheduleStatus(String schedulerType)throws DaoException;

	public List<BankCountryModel> getAllBanks()throws DaoException;

	public List<BankCodeModel> getBankDetailsByCountryCode(String countryCode)throws DaoException;

	public BankCodeModel getBankDetailsByReceiveBankCode(String bankCode)throws DaoException;



}
