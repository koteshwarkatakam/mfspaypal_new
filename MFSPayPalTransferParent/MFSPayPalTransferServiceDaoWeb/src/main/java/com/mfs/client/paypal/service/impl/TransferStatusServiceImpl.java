package com.mfs.client.paypal.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.dto.Login;
import com.mfs.client.dto.TransStatusRequestDto;
import com.mfs.client.dto.TransStatusResponseDto;
import com.mfs.client.paypal.dao.SystemConfigDao;
import com.mfs.client.paypal.dao.TransactionDao;
import com.mfs.client.paypal.dto.AuthResponseDto;
import com.mfs.client.paypal.exception.CommonException;
import com.mfs.client.paypal.exception.DaoException;
import com.mfs.client.paypal.models.TransferDetailsLogModel;
import com.mfs.client.paypal.service.AuthService;
import com.mfs.client.paypal.service.TransferAcknowledgementsService;
import com.mfs.client.paypal.service.TransfersStatusService;
import com.mfs.client.paypal.util.CommonConstant;
import com.mfs.client.paypal.util.MfsResponseCodes;
import com.mfs.client.paypal.util.ResponseCodes;
import com.mfs.client.service.TransStatusService;

@Service("TransfersStatusService")
public class TransferStatusServiceImpl implements TransfersStatusService {

	private static final Logger LOGGER = LogManager.getLogger(TransferStatusServiceImpl.class);

	@Autowired
	SystemConfigDao systemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	AuthService authService;

	@Autowired
	TransStatusService transStatusService;

	@Autowired
	TransferAcknowledgementsService transferAcknowledgementsService;

	public void getTransferStatus() {

		Map<String, String> configMap = null;
		List<TransferDetailsLogModel> allPendingRecords = new ArrayList<TransferDetailsLogModel>();
		TransStatusRequestDto request = new TransStatusRequestDto();
		TransStatusResponseDto response = null;

		try {

			// getting data from config table
			configMap = systemConfigDao.getConfigDetailsMap();

			if (configMap.get(CommonConstant.TRANS_STATUS_SWITCH).equalsIgnoreCase(CommonConstant.ON)) {

				// add retry count condition
				String retryCount = configMap.get(CommonConstant.RETRY_COUNT);
				int count = 1;
				if (retryCount != null && !retryCount.isEmpty()) {
					count = Integer.parseInt(retryCount);
				}

				// getting all pending transaction from db
				allPendingRecords = transactionDao.getPendingTransaction(count);

				if (allPendingRecords != null && !allPendingRecords.isEmpty()) {

					LOGGER.info("Pending list size is :: " + allPendingRecords.size());
					Login login = new Login();
					login.setCorporate_code(configMap.get(CommonConstant.CORPORATE_CODE));
					login.setPassword(configMap.get(CommonConstant.PASSWORD));
					request.setLogin(login);
					request.setWsdl(configMap.get(CommonConstant.WSDL));
					if (configMap.get(CommonConstant.X_API_KEY) != null) {
						request.setxApiKey(configMap.get(CommonConstant.X_API_KEY));
					}

					// calling get_trans_status for each pending transaction
					for (TransferDetailsLogModel record : allPendingRecords) {

						try {

							request.setTrans_id(record.getMfsTransId());

							response = new TransStatusResponseDto();

							LOGGER.debug(
									"==>In TransactionStatusServiceImpl of updateTransactionStatus function calling trans status "
											+ record.getMfsTransId());

							// calling mfsapiclient for getting mfs api response for trans_status
							response = transStatusService.getTransStatus(request);

							LOGGER.debug(
									"==>In TransactionStatusServiceImpl of updateTransactionStatus function mfs api client response :: "
											+ response);

							if (!response.getCode().getStatus_code().equalsIgnoreCase(MfsResponseCodes.MR102.getCode())
									&& !response.getCode().getStatus_code()
											.equalsIgnoreCase(MfsResponseCodes.MR103.getCode())
									&& !response.getCode().getStatus_code()
											.equalsIgnoreCase(MfsResponseCodes.MR108.getCode())) {

								// updating trans_status response in DB
								updateStatus(record.getMfsTransId(), response);

								// call to acknowledgements service
								transferAcknowledgementsService.transferAcknowledgements();

							}

						} catch (Exception e) {
							LOGGER.error(
									"==>Exception occured in TransactionStatusServiceImpl while processing status update for Paypal Trans Id="
											+ record.getTrackingNumber(),
									e);
						}
					}

				} else {
					LOGGER.info("==>In TransactionStatusServiceImpl of updateTransactionStatus function  "
							+ "In Database No Commit Pending Transaction Records Exists");
				}
			} else {
				LOGGER.error(
						"==>Trans Status Switch is off, please make sure to update SWITCH value for transaction status process");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateStatus(String mfsTransId, TransStatusResponseDto response) throws CommonException, DaoException {
		TransferDetailsLogModel transferDetails = transactionDao.getTransferDetails(mfsTransId);

		if (transferDetails != null) {

			if (response.getCode().getStatus_code().equalsIgnoreCase(MfsResponseCodes.MR101.getCode())) {

				transferDetails.setMfsStatus(ResponseCodes.COMMIT_SUCCESS.getMessage());
				transferDetails.setRespCode(response.getCode().getStatus_code());
				transferDetails.setRespMessage(response.getMessage());

			} else {

				if (response.getStatusMessage() != null) {
					transferDetails.setMfsStatus(ResponseCodes.COMMIT_FAIL.getMessage());
					transferDetails.setRespCode(response.getCode().getStatus_code());
					transferDetails.setRespMessage(response.getStatusMessage());
				} else {
					transferDetails.setMfsStatus(ResponseCodes.COMMIT_FAIL.getMessage());
					transferDetails.setRespCode(response.getCode().getStatus_code());
					transferDetails.setRespMessage(response.getMessage());
				}

			}
			transferDetails.setDateUpdated(new Date());
			transactionDao.update(transferDetails);
		}

	}

}
