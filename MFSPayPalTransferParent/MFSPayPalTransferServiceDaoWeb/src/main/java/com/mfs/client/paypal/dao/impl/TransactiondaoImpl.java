package com.mfs.client.paypal.dao.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.paypal.dao.TransactionDao;
import com.mfs.client.paypal.dto.ResponseStatus;
import com.mfs.client.paypal.exception.DaoException;
import com.mfs.client.paypal.models.AccessTokenModel;
import com.mfs.client.paypal.models.BankCodeModel;
import com.mfs.client.paypal.models.BankCountryModel;
import com.mfs.client.paypal.models.SchedulerLogModel;
import com.mfs.client.paypal.models.TransferDetailsLogModel;
import com.mfs.client.paypal.util.ResponseCodes;

@Repository("TransactionDao")
@EnableTransactionManagement
public class TransactiondaoImpl extends BaseDaoImpl implements TransactionDao {

	@Autowired
	SessionFactory sessionFactory;

	// private static final Logger LOGGER =
	// Logger.getLogger(TransactiondaoImpl.class);
	private static Logger LOGGER = LogManager.getLogger(TransactiondaoImpl.class);

	@Transactional
	public AccessTokenModel getAuthTokenDetailsByLastRecord() throws DaoException {
		AccessTokenModel accessTokenModel = null;
		Session session = sessionFactory.getCurrentSession();

		try {
			// accessTokenModel = (AccessTokenModel) session.createQuery("From
			// AccessTokenModel order by id DESC").list().get(0);
			String hql = "From AccessTokenModel order by id DESC";
			Query query = session.createQuery(hql);
			accessTokenModel = (AccessTokenModel) query.setMaxResults(1).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("==>Exception in TransactonDaoImpl", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(ResponseCodes.ER208.getCode());
			status.setStatusMessage(ResponseCodes.ER208.getMessage());
			throw new DaoException(status);
		}
		return accessTokenModel;
	}

	@Transactional
	public TransferDetailsLogModel getTransfersRecordBytrackingNumber(String trackingNumber) throws DaoException {
		TransferDetailsLogModel logModel = null;
		Session session = sessionFactory.getCurrentSession();

		try {
			logModel = (TransferDetailsLogModel) session
					.createQuery("From TransferDetailsLogModel where trackingNumber=:trackingNumber")
					.setParameter("trackingNumber", trackingNumber).uniqueResult();
		} catch (Exception e) {

			LOGGER.error("=> Error in TransactiondaoImpl of getRecordByMgiTransactionId function ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER270.getMessage());
			status.setStatusCode(ResponseCodes.ER270.getCode());
			throw new DaoException(status);
		}

		return logModel;
	}

	@Transactional
	public List<TransferDetailsLogModel> getAllQueuedTransaction(int count) throws DaoException {
		List<TransferDetailsLogModel> transferDetails = null;
		Session session = sessionFactory.getCurrentSession();

		try {

			/*
			 * Query query = session.createQuery(
			 * "from TransferDetailsLogModel  where dateUpdate > :date and retryCount < :retryCount and mfsStatus= :mfsStatus"
			 * );
			 * 
			 * query.setParameter("date", new Date(System.currentTimeMillis() -
			 * 24 * 60 * 60 * 1000)) .setParameter("retryCount", count)
			 * .setParameter("mfsStatus",
			 * ResponseCodes.COMMIT_QUEUED.getMessage());
			 */
			Query query = session.createQuery(
					"from TransferDetailsLogModel  where retryCount < :retryCount and mfsStatus= :mfsStatus");

			query.setParameter("retryCount", count).setParameter("mfsStatus", ResponseCodes.COMMIT_QUEUED.getMessage());

			LOGGER.info("Queued transaction list query -> " + query);

			transferDetails = query.list();

		} catch (Exception e) {

			LOGGER.error("=> Error in TransactiondaoImpl of getAllQueuedTransaction function ");
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER258.getMessage());
			status.setStatusCode(ResponseCodes.ER258.getCode());
			throw new DaoException(status);

		}

		return transferDetails;

	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<TransferDetailsLogModel> getAllSuccessAndFailRecord() throws DaoException {
		List<TransferDetailsLogModel> transferDetails = null;
		Session session = sessionFactory.getCurrentSession();

		try {

			String query = "FROM TransferDetailsLogModel where " + "mfsStatus= '"
					+ ResponseCodes.LOG_SUCCESS.getMessage() + "'" + " OR mfsStatus= '"
					+ ResponseCodes.LOG_FAIL.getMessage() + "'";

			transferDetails = (List<TransferDetailsLogModel>) session.createQuery(query).list();

		} catch (Exception e) {

			LOGGER.error("=> Error in TransactiondaoImpl of getRecordByMgiTransactionId function ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER270.getMessage());
			status.setStatusCode(ResponseCodes.ER270.getCode());
			throw new DaoException(status);
		}

		return transferDetails;
	}

	@Transactional
	public List<TransferDetailsLogModel> getPendingTransaction(final int retryCount) throws DaoException {

		LOGGER.debug("Inside TransactiondaoImpl of getPendingTransaction function");

		List<TransferDetailsLogModel> transferDetails = null;
		Session session = sessionFactory.getCurrentSession();

		try {
			String query = "FROM TransferDetailsLogModel " + "WHERE retryCount < " + retryCount + " AND  mfsStatus= '"
					+ ResponseCodes.COMMIT_PENDING.getMessage() + "'";
			LOGGER.debug("Penidng list query -> " + query);
			transferDetails = (List<TransferDetailsLogModel>) session.createQuery(query).list();
		} catch (Exception e) {

			LOGGER.error("=> Error in TransactiondaoImpl of getPendingTransaction function ");
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER208.getMessage());
			status.setStatusCode(ResponseCodes.ER208.getCode());
			throw new DaoException(status);

		}

		return transferDetails;

	}

	@Transactional
	public TransferDetailsLogModel getTransferDetails(String mfsTransId) throws DaoException {

		TransferDetailsLogModel transferDetails = null;
		Session session = sessionFactory.getCurrentSession();

		try {

			transferDetails = (TransferDetailsLogModel) session
					.createQuery("From TransferDetailsLogModel where mfsTransId= :mfsTransId")
					.setParameter("mfsTransId", mfsTransId).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("=> Error in TransactiondaoImpl of getTransferDetails function ");
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER208.getMessage());
			status.setStatusCode(ResponseCodes.ER208.getCode());
			throw new DaoException(status);

		}
		return transferDetails;
	}

	@Transactional
	public SchedulerLogModel getTransferScheduleStatus(String type) throws DaoException {

		SchedulerLogModel schedulerLogModel = null;
		Session session = sessionFactory.getCurrentSession();

		try {

			schedulerLogModel = (SchedulerLogModel) session.createQuery("From SchedulerLogModel where type= :type")
					.setParameter("type", type).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("=> Error in TransactiondaoImpl of getTransferScheduleStatus function " + e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER271.getMessage());
			status.setStatusCode(ResponseCodes.ER271.getCode());
			throw new DaoException(status);

		}
		return schedulerLogModel;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<BankCountryModel> getAllBanks() throws DaoException {
		List<BankCountryModel> bankDetails = null;
		Session session = sessionFactory.getCurrentSession();

		try {

			String query = "FROM BankCountryModel";

			bankDetails = (List<BankCountryModel>) session.createQuery(query).list();

		} catch (Exception e) {

			LOGGER.error("=> Error in TransactiondaoImpl of bankDetails function ", e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER270.getMessage());
			status.setStatusCode(ResponseCodes.ER270.getCode());
			throw new DaoException(status);
		}

		return bankDetails;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<BankCodeModel> getBankDetailsByCountryCode(String countryCode) throws DaoException {

		List<BankCodeModel> bankDetails = null;
		Session session = sessionFactory.getCurrentSession();

		try {

			bankDetails = (List<BankCodeModel>) session
					.createQuery("From BankCodeModel where countryCode= :countryCode")
					.setParameter("countryCode", countryCode).list();
		} catch (Exception e) {
			LOGGER.error("=> Error in TransactiondaoImpl of getBankCountries function ");
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER208.getMessage());
			status.setStatusCode(ResponseCodes.ER208.getCode());
			throw new DaoException(status);

		}

		return bankDetails;

	}

	@Transactional
	public BankCodeModel getBankDetailsByReceiveBankCode(String bankCode) throws DaoException {

		BankCodeModel transferDetails = null;
		Session session = sessionFactory.getCurrentSession();

		try {

			transferDetails = (BankCodeModel) session.createQuery("From BankCodeModel where bankCode= :bankCode")
					.setParameter("bankCode", bankCode).uniqueResult();
		} catch (Exception e) {
			LOGGER.error("=> Error in TransactiondaoImpl of getBankDetailsByReceiveCountryCode function ");
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(ResponseCodes.ER208.getMessage());
			status.setStatusCode(ResponseCodes.ER208.getCode());
			throw new DaoException(status);

		}
		return transferDetails;
	}

}