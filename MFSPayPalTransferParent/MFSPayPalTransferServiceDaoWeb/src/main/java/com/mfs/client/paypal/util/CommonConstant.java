package com.mfs.client.paypal.util;

public class CommonConstant {

	
	public static final String CONTENT_TYPE = "Content-type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String TOKEN = "token";
	public static final String TEXT_XML = "text/xml";
	public static final String AUTHORIZATION = "Authorization";
	
	public static final String SUCCESS_CODE = "200";
	public static final String DUPLICATERECORDERROR = "ER210";
	public static final String SYSTEMCONFIG_ERROR = "ER221";
	public static final String SYSTEMCOFIG_NULL_VALUES = "ER222";
	public static final String SYSTEMCONFIG_NULLVALUES = "ER223";
	public static final String BANK_DETAILS_ERROR = "ER230";
	public static final String BANKDATA_ERROR = "ER231";
	public static final String JAZZCASH_NULL_RESPONSE = "ER215";
	public static final String JAZZCASH_EXCEPTION = "ER207";
	public static final String RECORD_NOT_FOUND = "ER209";
	public static final String VALIDATION_ERROR = "ER206";
	public static final String REQUEST_PARSING_ERROR = "ER240";
	public static final String SHA1HASSHING_EXCEPTION = "ER256";
	

	
	

	
	//systemConfig
	public static final String BASE_URL = "base_url";
	public static final String SWITCH = "switch";
    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET = "client_secret";
	public static final String PORT = "port";
	public static final String XOOM_ISSUED_USERNAME = "xoom_issued_username";
	public static final String XOOM_ISSUED_PASSWORD = "xoom_issued_password";
	public static final String XOOM_ISSUED_GRANT_TYPE = "xoom_issued_grant_type";
	public static final String GET_TRANSFERS_ENDPOINT = "get_transfers_endpoint";
	public static final String AUTH_ENDPOINT = "auth_endpoint";
	public static final String PASSWORD = "password";
	public static final String CORPORATE_CODE = "corporate_code";
	public static final String WSDL = "mfs_url";
	public static final String X_API_KEY = "x-api-key";
	public static final String RETRY_COUNT = "retry_count";
	public static final String ACKNOWLEDGEMENT_ENDPOINT = "get_acknowledgements_endpoint";
	public static final String TRANS_STATUS_SWITCH = "trans_switch";
	public static final String BATCH_SIZE = "batch_size";
	public static final String ON = "on";


//////=========================================///////////////////
	public static final String APPLICATION_X_WWW_FORM_URLENCODED = "application/x-www-form-urlencoded";
	public static final String BASIC = "basic";
	public static final int EXPIRATION_TIME = 59;
	public static final String TOKEN_TYPE = "Bearer ";
	public static final String PAYPAL_NULL_RESPONSE = "PayPal null response";
	public static final String PAYPAL_NULL_RESPONSE_CODE = "ER260";
	public static final String MFS_LOG_FAIL_TRANS_ID = "00000000";
	//Scheduler
	public static final String SCHEDULER_ACTIVE = "Active";
	public static final String SCHEDULER_INACTIVE = "Inactive";
	public static final String GETTRANSFERS_SCHED_TYPE = "getTransfers";
	public static final String QUEUED_TRANS_SCHED_TYPE = "queuedTransactions";
	public static final String PENDING_ACKNOW_SCHED_TYPE = "pendingacknowledgement";
	public static final String GETBANKS_SCHED_TYPE = "getBanks";

}
