package com.mfs.client.paypal.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.dto.GetBanksRequestDto;
import com.mfs.client.dto.GetBanksResponseDto;
import com.mfs.client.dto.ListOfGetBanksResponseDto;
import com.mfs.client.dto.Login;
import com.mfs.client.paypal.dao.SystemConfigDao;
import com.mfs.client.paypal.dao.TransactionDao;
import com.mfs.client.paypal.exception.CommonException;
import com.mfs.client.paypal.exception.DaoException;
import com.mfs.client.paypal.models.BankCodeModel;
import com.mfs.client.paypal.models.BankCountryModel;
import com.mfs.client.paypal.service.BankCountyService;
import com.mfs.client.paypal.util.CommonConstant;
import com.mfs.client.service.GetBanksService;

@Service("BankCountyService")
public class BankCountyServiceImpl implements BankCountyService {
	private static final Logger LOGGER = LogManager.getLogger(BankCountyServiceImpl.class);
	@Autowired
	SystemConfigDao systemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	GetBanksService getBanksService;

	public void getBankCountries(String toCountry) {

		GetBanksRequestDto request = new GetBanksRequestDto();
		ListOfGetBanksResponseDto response = new ListOfGetBanksResponseDto();
		Map<String, String> configMap = null;

		try {

			List<BankCountryModel> getAllBanks = null;

			getAllBanks = transactionDao.getAllBanks();

			if (getAllBanks != null && !getAllBanks.isEmpty()) {
				for (BankCountryModel bank : getAllBanks) {

					// getting data from config table
					configMap = systemConfigDao.getConfigDetailsMap();
					Login login = new Login();
					login.setCorporate_code(configMap.get(CommonConstant.CORPORATE_CODE));
					login.setPassword(configMap.get(CommonConstant.PASSWORD));
					request.setLogin(login);
					request.setWsdl(configMap.get(CommonConstant.WSDL));
					if (configMap.get(CommonConstant.X_API_KEY) != null) {
						request.setxApiKey(configMap.get(CommonConstant.X_API_KEY));
					}

					request.setToCountry(bank.getCountryCode());

					// calling mfsapiclient for getting mfs api response for
					// get_banks
					response = getBanksService.getBanks(request);

					updateResponse(response, bank.getCountryCode());
				}
			} else {

				LOGGER.info("==>In BankCountyServiceImpl of updateBankCountries function  "
						+ "In Database no bank details found");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public List<BankCountryModel> getBankCountries() {

		List<BankCountryModel> getAllBanks = null;
		try {
			getAllBanks = transactionDao.getAllBanks();

		} catch (DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return getAllBanks;

	}

	private void updateResponse(ListOfGetBanksResponseDto response, String countryCode)
			throws CommonException, DaoException {
		List<BankCodeModel> bankDetails = transactionDao.getBankDetailsByCountryCode(countryCode);
		List<GetBanksResponseDto> list = response.getGetBanks();

		if (list != null) {
			for (GetBanksResponseDto bank : list) {
				if (bankDetails.isEmpty()) {

					BankCodeModel response1 = new BankCodeModel();

					response1.setBankName(bank.getBank_name());
					response1.setBankCode(bank.getMfs_bank_code());
					response1.setCountryCode(bank.getCountry_code());
					response1.setCurrencyCode(bank.getCurrency_code());
					response1.setIban(bank.getIban());
					response1.setBic(bank.getBic());
					transactionDao.save(response1);

				} else {
					String bankCode = bank.getMfs_bank_code();
					BankCodeModel model = transactionDao.getBankDetailsByReceiveBankCode(bankCode);
					model.setBankName(bank.getBank_name());
					model.setBankCode(bank.getMfs_bank_code());
					model.setCountryCode(bank.getCountry_code());
					model.setCurrencyCode(bank.getCurrency_code());
					model.setIban(bank.getIban());
					model.setBic(bank.getBic());
					transactionDao.update(model);
				}

			}
		}

	}

}
