package com.mfs.client.paypal.service;

import com.mfs.client.paypal.exception.ServiceException;

public interface PayPalTransferSchedulerService {

	String schedule(String string) throws ServiceException, Exception;

}
