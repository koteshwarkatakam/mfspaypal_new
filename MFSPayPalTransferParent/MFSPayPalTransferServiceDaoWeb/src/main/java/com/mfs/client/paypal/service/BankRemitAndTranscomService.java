package com.mfs.client.paypal.service;

import java.util.Map;

import com.mfs.client.dto.BankRemitLogRequestDto;
import com.mfs.client.paypal.dto.ItemsArrayResponseDto;
import com.mfs.client.paypal.exception.CommonException;
import com.mfs.client.paypal.exception.DaoException;

public interface BankRemitAndTranscomService {
	public void bankRemitAndTranscomCall(ItemsArrayResponseDto itemsArray, Map<String, String> configMap, BankRemitLogRequestDto remmitRequest, int transRetryCount) throws CommonException, DaoException;
}
