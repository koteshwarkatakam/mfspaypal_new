package com.mfs.client.paypal.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.dto.Account;
import com.mfs.client.dto.BankRemitLogRequestDto;
import com.mfs.client.dto.Document;
import com.mfs.client.dto.Login;
import com.mfs.client.dto.Receive_amount;
import com.mfs.client.dto.Recipient;
import com.mfs.client.dto.Sender;
import com.mfs.client.dto.Status;
import com.mfs.client.paypal.dao.SystemConfigDao;
import com.mfs.client.paypal.dao.TransactionDao;
import com.mfs.client.paypal.exception.CommonException;
import com.mfs.client.paypal.models.TransferDetailsLogModel;
import com.mfs.client.paypal.service.TransactionQueuedService;
import com.mfs.client.paypal.service.BankRemitAndTranscomService;
import com.mfs.client.paypal.util.CommonConstant;

@Service("TransactionQueuedService")
public class TransactionQueuedServiceImpl implements TransactionQueuedService {

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDao systemConfigDao;

	@Autowired
	BankRemitAndTranscomService bankRemitAndTranscomService;
	
	private static final Logger LOGGER = LogManager.getLogger(TransactionQueuedServiceImpl.class);

	public void queuedTransactionProcess() {

		List<TransferDetailsLogModel> queuedTransactionList = new ArrayList<TransferDetailsLogModel>();
		Map<String, String> systemConfig = null;
		BankRemitLogRequestDto bankRemitRequest = new BankRemitLogRequestDto();
		int transRetryCount = 0;

		try {

			// getting config data from db
			systemConfig = systemConfigDao.getConfigDetailsMap();

			String retryCount = systemConfig.get(CommonConstant.RETRY_COUNT);
			int count = 0;
			if (retryCount != null && !retryCount.isEmpty()) {
				count = Integer.parseInt(retryCount);
			}

			// getting all queued transaction from database
			queuedTransactionList = transactionDao.getAllQueuedTransaction(count);

			// initiating the transaction process for each queued transaction
			for (TransferDetailsLogModel TransferDetailsLogModel : queuedTransactionList) {

				// to get load request
				bankRemitRequest = bankRemmitRequest(TransferDetailsLogModel, systemConfig);

				transRetryCount = TransferDetailsLogModel.getRetryCount();

				// for queued transaction
				/*
				 * QueuedTransactionProcessStartThread
				 * queuedTransactionProcessStartThread = beanFac
				 * .getBean(QueuedTransactionProcessStartThread.class,
				 * bankRemitRequest, systemConfig, transRetryCount);
				 * queuedTransactionProcessStartThread
				 * .setName("queuedTransactionProcessStartThread" + new
				 * Date().getTime());
				 * taskExecutor.execute(queuedTransactionProcessStartThread);
				 */

				bankRemitAndTranscomService.bankRemitAndTranscomCall(null, systemConfig,bankRemitRequest,transRetryCount);

				LOGGER.info("Queued Transaction execution ended for tracking number :: "
						+ TransferDetailsLogModel.getTrackingNumber());

			}

		} catch (CommonException ce) {
			LOGGER.error("==>CommonException in TransactionQueuedServiceImpl of queuedTransactionProcess function", ce);
		} catch (Exception e) {
			LOGGER.error("==>Exception in TransactionQueuedServiceImpl of queuedTransactionProcess function", e);
		}

	}

	private BankRemitLogRequestDto bankRemmitRequest(TransferDetailsLogModel transferDetailsLogModel,
			Map<String, String> systemConfig) {

		BankRemitLogRequestDto mfsRequest = new BankRemitLogRequestDto();
		Document document = new Document();
		Document document1 = new Document();
		Recipient recipient = new Recipient();
		Receive_amount receive_amount = new Receive_amount();
		Login login = new Login();
		Sender sender = new Sender();
		String countryCode = null;

		login.setCorporate_code(systemConfig.get(CommonConstant.CORPORATE_CODE));
		login.setPassword(systemConfig.get(CommonConstant.PASSWORD));
		receive_amount.setAmount(Double.valueOf(transferDetailsLogModel.getReceiveAmount()));
		receive_amount.setCurrency_code(transferDetailsLogModel.getRecCurCode());

		if (transferDetailsLogModel.getSenderAddress() != null) {
			sender.setAddress(transferDetailsLogModel.getSenderAddress());
		} else {
			sender.setAddress("");
		}
		if (transferDetailsLogModel.getSenderCity() != null) {
			sender.setCity(transferDetailsLogModel.getSenderCity());
		} else {
			sender.setCity("");
		}
		// we dont get dob from paypal
		sender.setDate_of_birth("");
		document.setId_country("");

		// we dont get dob from Id_expiry
		document.setId_expiry("");

		// we dont get dob from Id_number
		document.setId_number("");
		// we dont get dob from Id_type
		document.setId_type("");
		sender.setDocument(document);

		if (transferDetailsLogModel.getSenderEmail() != null) {
			sender.setEmail(transferDetailsLogModel.getSenderEmail());
		} else {
			sender.setEmail("");
		}
		// 2 digit country code
		// countryCode =
		// transactionDao.getTwoDigitCountryCodeByCountryCode(request.getTransaction().getSendCountryCode());

		sender.setFrom_country(transferDetailsLogModel.getSenderCountry());

		if (transferDetailsLogModel.getSenderMsisdn() != null) {
			sender.setMsisdn(transferDetailsLogModel.getSenderMsisdn());
		} else {
			sender.setMsisdn("");
		}
		sender.setName(transferDetailsLogModel.getSenderFirstName());

		if (transferDetailsLogModel.getSenderPostalCode() != null) {
			sender.setPostal_code(transferDetailsLogModel.getSenderPostalCode());
		} else {
			sender.setPostal_code("");
		}

		if (transferDetailsLogModel.getSenderState() != null) {
			sender.setState(transferDetailsLogModel.getSenderState());
		} else {
			sender.setState("");
		}
		sender.setSurname(transferDetailsLogModel.getSenderLastName());

		if (transferDetailsLogModel.getReceiverAddress() != null) {
			recipient.setAddress(transferDetailsLogModel.getReceiverAddress());
		} else {
			recipient.setAddress("");
		}
		if (transferDetailsLogModel.getReceiverCity() != null) {
			recipient.setCity(transferDetailsLogModel.getReceiverCity());
		} else {
			recipient.setCity("");
		}

		recipient.setDate_of_birth("");
		document1.setId_country("");
		document1.setId_number("");
		document1.setId_type("");
		document1.setId_expiry("");
		recipient.setDocument(document1);

		if (transferDetailsLogModel.getReceiverEmail() != null) {
			recipient.setEmail(transferDetailsLogModel.getReceiverEmail());
		} else {
			recipient.setEmail("");
		}
		recipient.setMsisdn(transferDetailsLogModel.getReceiverMsisdn());
		recipient.setName(transferDetailsLogModel.getReceiverFirstName());

		if (transferDetailsLogModel.getReceiverPostalCode() != null) {
			recipient.setPostal_code(transferDetailsLogModel.getReceiverPostalCode());
		} else {
			recipient.setPostal_code("");
		}
		if (transferDetailsLogModel.getReceiverState() != null) {
			recipient.setState(transferDetailsLogModel.getReceiverState());
		} else {
			recipient.setState("");
		}
		Status status1 = new Status();
		status1.setCode("");
		recipient.setStatus(status1);
		recipient.setSurname(transferDetailsLogModel.getReceiverLastName());

		// 2 digit country code
		recipient.setTo_country(transferDetailsLogModel.getReceiverCountry());
		Account account = new Account();
		account.setAccount_number(transferDetailsLogModel.getAccountNumber());

		// MFSBankDetail bankCode =
		// transactionDao.getMfsBankCodeByRouterCode(request.getRoutingCode());

		account.setMfs_bank_code(transferDetailsLogModel.getBankCode());

		mfsRequest.setLogin(login);
		mfsRequest.setReceive_amount(receive_amount);
		mfsRequest.setSender(sender);
		mfsRequest.setRecipient(recipient);
		mfsRequest.setAccount(account);
		mfsRequest.setThird_party_trans_id(transferDetailsLogModel.getTrackingNumber());
		mfsRequest.setReference("");
		mfsRequest.setWsdl(systemConfig.get(CommonConstant.WSDL));
		if (systemConfig.get(CommonConstant.X_API_KEY) != null) {
			mfsRequest.setxApiKey(systemConfig.get(CommonConstant.X_API_KEY));
		}
		return mfsRequest;

	}

}
