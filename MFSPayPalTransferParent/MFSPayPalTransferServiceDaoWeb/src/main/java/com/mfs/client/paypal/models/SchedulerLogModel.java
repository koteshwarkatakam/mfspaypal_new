package com.mfs.client.paypal.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name=" scheduler_log")
public class SchedulerLogModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int schedulerId;
	@Column(name = "exec_timestamp")
	private Date execTimestamp;
	@Column(name = "status")
	private String status;
	@Column(name = "type")
	private String type;
	
	public int getSchedulerId() {
		return schedulerId;
	}
	public void setSchedulerId(int schedulerId) {
		this.schedulerId = schedulerId;
	}
	
	public Date getExecTimestamp() {
		return execTimestamp;
	}
	public void setExecTimestamp(Date execTimestamp) {
		this.execTimestamp = execTimestamp;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "SchedulerLogModel [schedulerId=" + schedulerId + ", execTimestamp=" + execTimestamp + ", status="
				+ status + ", type=" + type + "]";
	}
	
	

}
