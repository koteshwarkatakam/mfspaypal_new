package com.mfs.client.paypal.controller;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mfs.client.paypal.dto.AuthResponseDto;
import com.mfs.client.paypal.dto.PayPalAcknowledgementResponseDto;
import com.mfs.client.paypal.dto.PayPalTransferResponseDto;
import com.mfs.client.paypal.service.AuthService;
import com.mfs.client.paypal.service.BankCountyService;
import com.mfs.client.paypal.service.PayPalTransferService;
import com.mfs.client.paypal.service.TransferAcknowledgementsService;
import com.mfs.client.paypal.service.TransfersStatusService;

@RestController
public class PayPalController {
	private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(PayPalController.class);
	@Autowired
	AuthService authService;

	@Autowired
    PayPalTransferService payPalTransferService;

	@Autowired
	TransferAcknowledgementsService transferAcknowledgementsService;

	@Autowired
	TransfersStatusService transfersStatusService;
	
	@Autowired
	BankCountyService bankCountyService;

	@GetMapping(value = "/messgae")
	public String getMessage() {

		return "Welcome";
	}

	@PostMapping("/authAccessToken")
	public AuthResponseDto authAccessToken() {
		AuthResponseDto response = new AuthResponseDto();

		response = authService.getAccessToken();
		return response;
	}

	@GetMapping("/getTransfers")
	public PayPalTransferResponseDto getTransfers() {
		
		LOGGER.debug("Debug Message Logged !!!");
        LOGGER.info("Info Message Logged !!!");
        LOGGER.trace("In trace log");
        
		PayPalTransferResponseDto response=payPalTransferService.getDepositTransfers();
		
		
		return response;
	}

	@GetMapping("/getAcknowledgements")
	public PayPalAcknowledgementResponseDto getAcknowledgements() {

		PayPalAcknowledgementResponseDto response = transferAcknowledgementsService.transferAcknowledgements();

		return response;
	}

	@GetMapping("/getPendingTransferStatus")
	public String getPendingTransferStatus() {

		transfersStatusService.getTransferStatus();
		
		return "Success";
	}
	
	@GetMapping("/getBanks/{toCountry}")
	public String getBanks(@PathVariable String toCountry) {

		bankCountyService.getBankCountries(toCountry);
		
		return "Success";
	}

}
