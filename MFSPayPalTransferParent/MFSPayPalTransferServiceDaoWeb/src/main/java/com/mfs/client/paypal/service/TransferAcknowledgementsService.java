package com.mfs.client.paypal.service;

import com.mfs.client.paypal.dto.PayPalAcknowledgementResponseDto;

public interface TransferAcknowledgementsService {

	public PayPalAcknowledgementResponseDto transferAcknowledgements();

}
