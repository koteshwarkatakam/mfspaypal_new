package com.mfs.client.paypal.dto;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public class PayPalTransferResponseDto {

	private String total_items;

	private String total_pages;

	private List<ItemsArrayResponseDto> items;

	private LinksDto links;

	private RequestDto request;
	
	private List<ErrorArrayResponse>errors;

	private String errorMessage;

	private String errorCode;

	public String getTotal_items() {
		return total_items;
	}

	public void setTotal_items(String total_items) {
		this.total_items = total_items;
	}

	public String getTotal_pages() {
		return total_pages;
	}

	public void setTotal_pages(String total_pages) {
		this.total_pages = total_pages;
	}

	public List<ItemsArrayResponseDto> getItems() {
		return items;
	}

	public void setItems(List<ItemsArrayResponseDto> items) {
		this.items = items;
	}

	public LinksDto getLinks() {
		return links;
	}

	public void setLinks(LinksDto links) {
		this.links = links;
	}

	public RequestDto getRequest() {
		return request;
	}

	public void setRequest(RequestDto request) {
		this.request = request;
	}

	public List<ErrorArrayResponse> getErrors() {
		return errors;
	}

	public void setErrors(List<ErrorArrayResponse> errors) {
		this.errors = errors;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return "PayPalTransferResponseDto [total_items=" + total_items + ", total_pages=" + total_pages + ", items="
				+ items + ", links=" + links + ", request=" + request + ", errors=" + errors + ", errorMessage="
				+ errorMessage + ", errorCode=" + errorCode + "]";
	}

	

}
