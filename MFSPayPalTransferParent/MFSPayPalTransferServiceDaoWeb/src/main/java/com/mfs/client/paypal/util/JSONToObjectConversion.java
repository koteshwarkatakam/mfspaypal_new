package com.mfs.client.paypal.util;



import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

public class JSONToObjectConversion {

	private static final Logger LOGGER = LogManager.getLogger(JSONToObjectConversion.class);

	public static Object getObjectFromJson(String json, Class<?> responseClass) {
		LOGGER.debug("JSONToObjectConversion for String => " + json);
		Gson gson = new Gson();
		Object obj = gson.fromJson(json, responseClass);
		return obj;
	}
}
