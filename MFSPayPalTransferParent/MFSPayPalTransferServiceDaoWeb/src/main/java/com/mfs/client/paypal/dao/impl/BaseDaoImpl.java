package com.mfs.client.paypal.dao.impl;




import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.paypal.dao.BaseDao;
import com.mfs.client.paypal.dto.ResponseStatus;
import com.mfs.client.paypal.exception.CommonException;
import com.mfs.client.paypal.util.ResponseCodes;


public class BaseDaoImpl implements BaseDao {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger LOGGER = LogManager.getLogger(BaseDaoImpl.class);
	//private static Logger LOGGER = LogManager.getLogger(BaseDaoImpl.class);

	@Transactional
	public boolean save(Object obj) throws CommonException {
		// LOGGER.debug("Inside BaseDAO Save");

		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().save(obj);
			isSuccess = true;
		} catch (Exception e) {

			LOGGER.error("==>Exception thrown in BaseDaoImpl in save " , e);
			ResponseStatus status = new ResponseStatus();

			status.setStatusCode(ResponseCodes.ER204.getCode());
			status.setStatusMessage(ResponseCodes.ER204.getMessage());

			throw new CommonException(status);

		}
		return isSuccess;
	}

	@Transactional
	public boolean saveOrUpdate(Object obj) throws CommonException {
		// LOGGER.debug("Inside BaseDAO saveOrUpdate");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().saveOrUpdate(obj);
			isSuccess = true;
		} catch (Exception e) {
			LOGGER.error("Exception in BaseDaoImpl in saveOrUpdate " , e);
			throw new CommonException("");
		}
		return isSuccess;
	}

	@Transactional
	public boolean update(Object obj) throws CommonException {
		// LOGGER.debug("Inside BaseDAO Update");
		boolean isSuccess = false;
		try {
			this.sessionFactory.getCurrentSession().update(obj);
			isSuccess = true;
		} catch (Exception e) {
			LOGGER.error("==>Exception thrown in BaseDaoImpl in update" , e);
			ResponseStatus status = new ResponseStatus();
			status.setStatusCode(ResponseCodes.ER205.getCode());
			status.setStatusMessage(ResponseCodes.ER205.getMessage());
			throw new CommonException(status);
		}
		return isSuccess;
	}

}
