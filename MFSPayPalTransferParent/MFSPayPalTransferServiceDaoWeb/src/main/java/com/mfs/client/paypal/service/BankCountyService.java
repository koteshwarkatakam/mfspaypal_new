package com.mfs.client.paypal.service;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;

import com.mfs.client.paypal.models.BankCountryModel;



public interface BankCountyService {
	
	public void getBankCountries(String toCountry);
	
	//public List<BankCountryModel> getBankCountries();

}
